<?php

namespace console\controllers;

use Imagine\Image\Box;
use Yii;
use yii\console\Controller;

class ResizeController extends Controller
{
    public function actionProducts()
    {
        $path = Yii::getAlias('@backend') . '/web/images/products/';

        $files = $this->dirToArray($path);

        foreach ($files as $file){
            $imagine = new \Imagine\Gd\Imagine();
            $imagine->open($path . $file)->resize(new Box(510, 402))
                ->save($path . $file, ['quality' => 80]);
        }

        return 1;
    }

    public function actionCatalog()
    {
        $path = Yii::getAlias('@backend') . '/web/images/catalog/';

        $files = $this->dirToArray($path);

        foreach ($files as $file){
            $imagine = new \Imagine\Gd\Imagine();
            $imagine->open($path . $file)->resize(new Box(508, 435))
                ->save($path . $file, ['quality' => 80]);
        }

        return 1;
    }

    public function dirToArray($dir)
    {
        $result = [];

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value,array(".","..")))
            {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                {
                    $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                }
                else
                {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }
}