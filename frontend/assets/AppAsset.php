<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       "/css/bootstrap.css",
       "/css/owl.carousel.min.css",
       "/css/owl.theme.default.min.css",
       "/css/animate.css",
       "/css/style.css",
       "/css/glide.core.css",
       "/css/glide.theme.css",
       "/css/awesome-bootstrap-checkbox.css",
       "https://use.fontawesome.com/releases/v5.7.1/css/all.css",
       "https://fonts.googleapis.com/css?family=Scada&display=swap",
    ];
    public $js = [
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "/js/bootstrap.min.js",
        "/js/wow.min.js",
        "/js/owl.carousel.min.js",
        "/js/glide.js",
        "/js/catalog.js",
        "/js/basket.js",
        "/js/account.js",
        "/js/vacancy.js",
        "/js/main.js",

    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
