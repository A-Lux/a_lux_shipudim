<?php

namespace frontend\controllers;

use common\models\Menu;
use common\models\Products;
use yii\web\Controller;

class SearchController extends FrontendController
{
    public $layout = "content";

    public function actionIndex($text)
    {
        $model = Menu::find()->where('url = "/search"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $menu = Menu::find()->all();

        $search = $text;
		if($text){
			$product = Products::find()->where("name LIKE '%$text%'")->all();
			$count = count($product);
        }else{
		    $count = 0;
			return $this->render('index', compact('search', 'count', 'text', 'menu'));
		}

        return $this->render('index', compact('product','count','search', 'menu'));
    }
}