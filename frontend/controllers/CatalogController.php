<?php
namespace frontend\controllers;

use common\models\Menu;
use common\models\Catalog;
use common\models\MainSub;
use common\models\Products;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\NotFoundHttpException;

/**
 * Catalog controller
 */
class CatalogController extends FrontendController
{
    public $layout = 'content';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function actionIndex($url)
    {
        $menu = Menu::find()->all();
        $mainSub = MainSub::find()->all();

        $catalog = Catalog::findOne(['url' => $url]);
        $this->setMeta($catalog->metaName, $catalog->metaDesc, $catalog->metaKey);

        $products = Products::getCatalog($catalog->id, 8);
        
        return $this->render('index', compact('catalog', 'menu', 'mainSub', 'products'));
    }

    public function actionSales()
    {
        $menu = Menu::find()->all();
        $model = Menu::find()->where('url = "/catalog/sales"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $product = Products::getAll(4);

        return $this->render('sales', compact('menu', 'product'));
    }

    public function actionProducts()
    {
        $id = $_GET['id'];
        $menu = Menu::find()->all();
        $catalog = Catalog::find()->where('status=1 AND level=1')->all();
        $catalogs = Catalog::find()->where('id='.$id)->one();

        $products = $catalogs->products;

        return $this->renderAjax('products',compact('products', 'catalog', 'menu'));
    }

    public function actionCategory($id)
    {
        $menu = Menu::find()->all();
        $catalog = Catalog::find()->where('status=1 AND level=1')->all();
        $catalogs = Catalog::find()->where('id='.$id)->all();
        $this->setMeta($catalogs->metaName, $catalogs->metaKey, $catalogs->metaDesc);

        return $this->render('category', compact('catalog', 'catalogs', 'menu'));
    }

    public function actionProductCard($url)
    {
        $product = Products::findOne(['url' => $url]);
        $this->setMeta($product->metaName, $product->metaDesc, $product->metaKey);
        $menu = Menu::find()->all();

        return $this->render('product-card', compact('product', 'menu'));
    }
    
}
