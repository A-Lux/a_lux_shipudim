<?php
namespace frontend\controllers;



use common\models\Banner;
use common\models\Address;
use common\models\Catalog;
use common\models\Mainsub;
use common\models\Menu;
use common\models\Products;
use common\models\Partners;
use DateTime;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use yii\helpers\ArrayHelper;
use common\models\SliderCart;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@frontend/web/fonts/Century Gothic.ttf',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionIndex()
    {
        $model      = Menu::find()->where('url = "/"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);
        $titles = Mainsub::find()->all();
        $banner = Banner::find()->all();
        $address = Address::find()->all();
        $parnters = Partners::find()->all();
        $catalog = Catalog::find()->all();
        $mainSub = Mainsub::find()->all();
        $slider = SliderCart::find()->all();

        return $this->render('index',compact('titles','banner', 'address', 'parnters', 'catalog',
            'mainSub', 'slider'));
    }


    public function actionTest()
    {
        $model = new Catalog();
        $model->name = 'Шашлык ассорти';

        $text = $model->generateCyrillicToLatin();

        var_dump($text);
    }


}
