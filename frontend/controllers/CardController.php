<?php

namespace frontend\controllers;


use app\models\CatalogProducts;
use common\models\Catalog;
use common\models\Emailforrequest;
use common\models\Geocoords;
use common\models\Menu;
use common\models\OrderedProduct;
use common\models\OrderInfo;
use common\models\Orders;
use common\models\Products;
use common\models\User;
use common\models\UserProfile;
use Yii;

class CardController extends FrontendController
{
    public $layout = "content";

    public function actionIndex()
    {
        $menu = Menu::find()->all();
        $model = Menu::find()->where('url = "/card"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);
        $sum = Products::getSum();

        return $this->render('basket',compact('model', 'menu', 'sum'));
    }

    public function actionDeleteProductAjax($id)
    {
        $m=0;
        foreach ($_SESSION['basket'] as $v){
            if($v->id == $id){
                unset($_SESSION['basket'][$m]);
                $_SESSION['basket'] = array_values($_SESSION['basket']);
                break;
            }
            $m++;
        }

        return 1;
    }

    public function actionSumBasket()
    {
        $sum = Products::getSum();

        return $sum;
    }


    public function actionDeleteAll()
    {
        unset($_SESSION['basket']);

        return $this->redirect(['index']);
    }

    public function actionAddProduct()
    {
        $id = $_GET['id'];
        $amount = $_GET['amount'];
        $basket = Products::findOne($id);
        $basket->count = $amount ? $amount : 1;
        $check = true;
        foreach ($_SESSION['basket'] as $v){
            if($v->id == $id){
                $check = false;
                break;
            }
        }
        if($check){
            array_push($_SESSION['basket'],$basket);
        }
        $count = count($_SESSION['basket']);
        $array = ['status' => 1,'count'=>$count];

        return json_encode($array);

    }

    public function actionDownClicked()
    {

        $count = 1;
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id'] && $_SESSION['basket'][$k]->count > 1){
                $_SESSION['basket'][$k]['count']-=1;
                $count = $_SESSION['basket'][$k]['count'];
                break;
            }
        }

        $_SESSION['productPrice'] = Products::getSum();
        $sum = $_SESSION['productPrice']+(int)$_SESSION['deliveryPrice'];
        if(isset($_SESSION['promo_id'])) {
            $sum = $sum * (100 - $_SESSION['promo']) / 100;
        }
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => intval($sum),'sumProduct'=>$sumProduct];
        return json_encode($array);
    }


    public function actionUpClicked()
    {
        $count = 1;
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id']){
                $_SESSION['basket'][$k]['count']+=1;
                $count = $_SESSION['basket'][$k]['count'];
                break;
            }
        }

        $_SESSION['productPrice'] = Products::getSum();
        $sum = $_SESSION['productPrice']+(int)$_SESSION['deliveryPrice'];
        if(isset($_SESSION['promo_id'])) {
            $sum = $sum * (100 - $_SESSION['promo']) / 100;
        }
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => intval($sum),'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionCountChanged()
    {
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id']){
                if($_GET['v']>0){
                    $_SESSION['basket'][$k]['count'] = $_GET['v'];
                }else{
                    $_SESSION['basket'][$k]['count']=1;
                }
            }
        }

        $_SESSION['productPrice'] = Products::getSum();
        $sum = $_SESSION['productPrice']+(int)$_SESSION['deliveryPrice'];

        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['sum' => intval($sum),'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionUpdateSession()
    {
        $_SESSION['deliveryPrice'] = $_GET['deliveryPrice'];
        $_SESSION['deliveryTime'] = $_GET['deliveryTime'];
        $_SESSION['productPrice'] = Products::getSum();
        $sum = (int)$_SESSION['deliveryPrice']+(int)$_SESSION['productPrice'];

        return intval($sum);
    }

    public function actionOrder()
    {
        if(empty($_SESSION['basket'])){
            $model = Menu::find()->where('url = "/card"')->one();
            $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
            return $this->render('basket');
        }else {
            $model = Menu::find()->where('url = "/card/order"')->one();
            $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
            $menu = Menu::find()->all();
            $product_id = time();

            $profile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
            $user = User::findOne(Yii::$app->user->id);

            $_SESSION['productPrice'] = Products::getSum();
            $_SESSION['deliveryPrice'] = 0;
            $sum = $_SESSION['productPrice'];

            $info = OrderInfo::find()->all();

            $geocoords = Geocoords::find()->all();

            return $this->render('order', compact('sum', 'geocoords', 'info', 'menu',
                'profile', 'user', 'product_id'));
        }
    }

    public function actionOrderByCourier()
    {
        $sum = (int)$_SESSION['productPrice'];

        $id = $_GET['order_id'];
        $order = new Orders();


        if($sum){
            $order->user_id = Yii::$app->user->id ? Yii::$app->user->id : 0;
            $order->product_id = $id;
            $order->username = $_GET['username'];
            $order->phone = $_GET['phone'];
            $order->statusPay = 1;
            $order->statusProgress = 1;
            $order->sum = $sum;
            $order->typePay = $_GET['typePay'];
            $order->typeDelivery = 1;
            $order->change = $_GET['change'] ? $_GET['change'] : 0;
            $order->address = $_GET['address'];
            $order->commentOrder = $_GET['comment'];


            if($order->save(false)){
                if($_SESSION['basket'] != null){
                    foreach ($_SESSION['basket'] as $v) {
                        $orderedProduct = new Orderedproduct();
                        $orderedProduct->order_id = $id;
                        $orderedProduct->product_id = $v->id;
                        $orderedProduct->count = $v->count;
                        $orderedProduct->save(false);
                    }
                }
                $user = User::findOne(['id' => Yii::$app->user->id]);
                if($user) {
                    $this->sendInformationAboutPayment($order->username, $user->email, $order->product_id, $sum);
                }
                $this->sendOrderOperator($order->product_id, $order->commentOrder, $order->username, $order->phone, $order->address);

                unset($_SESSION['basket']);
                unset($_SESSION['deliveryTime']);
                unset($_SESSION['deliveryPrice']);
                unset($_SESSION['productPrice']);

                return 1;
            }
        }else {
            $error = "";
            $errors = $order->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];
                break;
            }
            return $error;
        }
    }

    public function actionTest()
    {
        $emailforrequest = Emailforrequest::find()->all();

        var_dump($emailforrequest);
    }

}