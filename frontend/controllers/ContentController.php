<?php

namespace frontend\controllers;

use common\models\About;
use common\models\PayDelivery;
use common\models\Respond;
use common\models\RespondFiles;
use common\models\UserProfile;
use common\models\Vacancy;
use Yii;
use common\models\Menu;
use common\models\Products;
use yii\web\UploadedFile;

class ContentController extends FrontendController
{
    public $layout = "content";

    public function actionVacancy()
    {
        $model = Menu::find()->where('url = "/content/vacancy"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);
        $menu = Menu::find()->all();

        $vacancy = Vacancy::getAll(6);
        $vacancies = Vacancy::find()->all();
        $userProfile = UserProfile::findOne(['id' => Yii::$app->user->id]);

        return $this->render('vacancy', compact('model', 'menu', 'vacancy', 'vacancies',
            'userProfile'));
    }

    public function beforeAction($action)
    {
        if($action->id == 'form-files'){
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionAbout()
    {
        $model = Menu::find()->where('url = "/content/about"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $about = About::findOne(['id' => 1]);

        return $this->render('about', compact('about'));
    }

    public function actionPayDelivery()
    {
        $model = Menu::find()->where('url = "/content/pay-delivery"')->one();
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $payDelivery = PayDelivery::findOne(['id' => 1]);

        return $this->render('pay-delivery', compact('payDelivery'));
    }

    public function actionRespond()
    {
        $model = new Respond();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $vacancy = Vacancy::findOne(['id' => $model->vacancy_id]);
                if($model->save() && $this->sendEmailVacancy($vacancy->title, $model->name, $model->phone, $model->text)){
                    if(!empty($_POST['files'])) {
                        foreach ($_POST['files'] as $v) {
                            $file = RespondFiles::findOne($v);
                            $file->parent_id = $model->id;
                            $file->save();
                        }

                        return 1;
                    }else{
                        return 2;
                    }
                }
            } else {
                $error = "";
                $errors = $model->getErrors();

                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }



    public function actionFormFiles()
    {
        if(isset($_FILES['Files'])){
            $files = $this->actionUpdateFile($_FILES['Files'], 'Files', 'resume');

            foreach($files['Files'] as $k => $v){
                $model = new RespondFiles();
                $model->name = $v;
                $model->save();
            }

            $id = $model->id;

            return $this->renderAjax('FormFiles', compact('id'));
        }
    }

    public function actionUpdateFile($FILES, $type, $dir)
    {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/frontend/web/uploads/resume';

        //if(!is_dir($uploaddir))
        //    mkdir($uploaddir, 0777);

        $array = [];
        foreach( $FILES as $k => $files ){
            foreach($files as $k1 => $file) {
                if (!empty($FILES['tmp_name'][$k1])) {
                    if (move_uploaded_file($FILES['tmp_name'][$k1], $uploaddir . '/' . time() . '_' . basename($FILES['name'][$k1]))) {
                        $array[$type][$k1] = time() . '_' . basename($FILES['name'][$k1]);
                    }
                }
            }
        }

        return $array;
    }
}