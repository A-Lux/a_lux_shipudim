<?php

namespace frontend\controllers;

use app\models\Auth;
use common\models\ForgotYourPassword;
use common\models\Menu;
use common\models\Orders;
use common\models\Products;
use common\models\UserFavorites;
use frontend\models\LoginForm;
use common\models\User;
use common\models\UserAddress;
use common\models\UserProfile;



use frontend\models\SignupForm;
use Yii;
use yii\debug\models\search\Profile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class AccountController extends FrontendController
{
    public $layout = 'content';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile'],
                'rules' => [

                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->goHome();
        }
        $menu = Menu::find()->all();
        $model = Menu::findOne(['url'=>'/account']);
        $this->setMeta($model->metaName, $model->metaDesc,$model->metaKey);

        $user = User::findOne(Yii::$app->user->id);
        $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
        $userAddress = UserAddress::findOne(['user_id' => Yii::$app->user->id]);
        $orders = Orders::getAll(5);
        $isOrder = Orders::find()->where(['user_id' => Yii::$app->user->id])->all();

        $favorites = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
        if($favorites != null){
            $arr = '(';
            foreach ($favorites as $v){
                $arr.=$v->product_id.',';
            }
            $arr = substr($arr,0,strlen($arr)-1);
            $arr.= ')';
            $fav = Products::find()->where('id in '.$arr)->all();
        }



        return $this->render('profile', compact('user','menu', 'profile', 'userAddress', 'fav', 'orders',
            'isOrder'));
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->login()) {
                return 1;
            }else{
                $error = "";
                $errors = $model->getErrors();

                foreach($errors as $v){
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionRegister()
    {
        $model = new SignupForm();
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post()) && $user = $model->signup()) {
                $password = $model->password;
                Yii::$app->getUser()->login($user);
                $this->sendInformationAboutRegistration($user->email, $password);
                return 1;
            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionUpdateAccountData()
    {
        $user = User::findOne(Yii::$app->user->id);
        $user->scenario = User::update_all_data;
        $profile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);

        if($profile){
            return $this->userProfileUpdate($user, $profile);
        }else{
            return $this->userProfileEntry($user);
        }


    }

    public function UserProfileEntry($user)
    {
        $profile = new UserProfile();

        if($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())){
            $profile->user_id = Yii::$app->user->id;
            if($profile->validate()) {
                if ($user->password != null) {
                    $user->scenario = User::update_all_data;
                } else {
                    $user->scenario = User::update_data;
                }

                if ($user->validate()) {
                    $password = $user->password;

                    if ($user->password != null) {
                        $user->setPassword($user->password);
                        $this->sendInformationAboutNewPassword($user->email, $password);
                    }
                    if ($profile->save() && $user->save()) {
                        return 1;
                    }
                }else{
                    $user_error = "";
                    $user_errors = $user->getErrors();
                    foreach($user_errors as $v){
                        $user_error .= $v[0];
                        break;
                    }
                    return $user_error;
                }
            }else{
                $profile_error = "";
                $profile_errors = $profile->getErrors();
                foreach($profile_errors as $v){
                    $profile_error .= $v[0];
                    break;
                }
                return $profile_error;
            }
        }
    }

    public function UserProfileUpdate($user, $profile)
    {
        if ($user->load(Yii::$app->request->post())&& $profile->load(Yii::$app->request->post()) && $profile->validate()) {

            if($user->password != null){
                $user->scenario = User::update_all_data;
            }else{
                $user->scenario = User::update_data;
            }

                if($user->validate()){
                    $password = $user->password;
                    if($user->password != null) {
                        $user->setPassword($user->password);
                        $this->sendInformationAboutNewPassword($user->email, $password);
                    }
                    if($profile->save() && $user->save()) {
                        return 1;
                    }
                }else{
                    $user_error = "";
                    $user_errors = $user->getErrors();
                    foreach($user_errors as $v){
                        $user_error .= $v[0];
                        break;
                    }
                    return $user_error;
                }

        }else{
            $profile_error = "";
            $profile_errors = $profile->getErrors();
            foreach($profile_errors as $v){
                $profile_error .= $v[0];
                break;
            }
            return $profile_error;
        }
    }


    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionForgotPassword()
    {
        $user = new ForgotYourPassword();
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {

            $check = User::findOne(['email'=>$user->email]);
            if($check) {
                $password = $check->getRandomPassword();
                $check->setPassword($password);
                if($check->save(false)){
                    if($this->sendInformationForgotYourPassword($user->email, $password)){
                        return 1;
                    }
                }
            }else{
                return 'Пользователь с таким e-mail отсутствует в базе!';
            }
        }else{
            $user_error = "";
            $user_errors = $user->getErrors();
            foreach($user_errors as $v){
                $user_error .= $v[0];
                break;
            }
            return $user_error;
        }
    }


    public function actionAddToFavorite()
    {
        $user_id = $_GET['user_id'];
        $product_id = $_GET['product_id'];
        $favorite = UserFavorites::find()->where('user_id='.$user_id.' AND product_id='.$product_id)->one();

        $text = "";
        if($favorite == null){
            $fav = new UserFavorites();
            $fav->user_id = $user_id;
            $fav->product_id = $product_id;
            if($fav->save()){
                $text = 'Товар добавлен в избранное!';
            }

        }else{
            $favorite->delete();
            $text = 'Товар удален с избранного!';
        }


        $array = ['text' => $text];
        return json_encode($array);

    }

    public function actionDeleteFromFavorite()
    {
        $favorite = UserFavorites::find()->where('user_id='.Yii::$app->user->id.' AND product_id='.$_GET['id'])->one();

        if($favorite->delete()){
            $favorites = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
            if($favorites != null){
                $arr = '(';
                foreach ($favorites as $v){
                    $arr.=$v->product_id.',';
                }
                $arr = substr($arr,0,strlen($arr)-1);
                $arr.= ')';
                $fav = Products::find()->where('id in '.$arr)->all();
            }
            return $this->renderAjax('favorite', compact('fav','error'));
        }else{
            return 0;
        }
    }



}
