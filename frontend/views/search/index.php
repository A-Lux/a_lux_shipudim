<?php

use yii\helpers\Url;

Yii::$app->view->params['title'] = "Поиск";
Yii::$app->view->params['desc'] = "Поиск";
Yii::$app->view->params['key'] = "Поиск";

?>

<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow fadeInLeft">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="<?=$menu[11]->url;?>"><?=$menu[11]->text;?></a></li>
                                </ul>
                            </div>
                            <div class="inner-title">
                                <?php
                                if($count){?>
                                    <div class="" style="color: #FFFFFF; font-size: 22px;margin-bottom: 30px;margin-right: 500px;">Результаты поиска по запросу <span style="color: #fff343; font-weight: bold;"><?=$search?></span>.</div>
                                <?}else{?>
                                    <div class="" style="color: #FFFFFF; font-size: 22px;margin-bottom: 300px;">По запросу <span style="color: #fff343; font-weight: bold;"><?=$search?></span> ничего не найдено.</div>
                                <?}?>
                            </div>
                        </div>

                        <? if(count($product) != 0):?>

                            <?foreach($product as $v):?>
                            <div class="col-sm-3 wow rollIn">
                                <div class="catalog-item">
                                    <div class="image">
                                        <div class="like">
                                            <a class="btn-add-favorite" data-id="<?=$v->id;?>"
                                               data-user-id="<?=Yii::$app->user->id?>">
                                                <img src="/images/like.png" alt=""></a>
                                        </div>
                                        <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'id' => $v->id])?>">
                                            <img src="<?=$v->getImage();?>" alt="" class="img-fluid"></a>
                                    </div>
                                    <div class="text-block">
                                        <h6><?=$v->name;?></h6>
                                        <p><?=$v->content;?></p>
                                        <h6><?=$v->price;?> тг</h6>
                                        <div class="order-block ">
                                            <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                                Заказать
                                                <img src="/images/basket-ico.svg" alt="">
                                            </button>
                                            <div class="quantity">
                                                <input class="count<?=$v->id;?>" type="number" min="1" name="count"  value="1">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?endforeach;?>

                        <? endif;?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
