<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow slideInLeft">
                                <ul>
                                    <li><a href="">Главная</a></li>
                                    <li>/</li>
                                    <li><a href="">Каталог</a></li>
                                    <li>/</li>
                                    <li><a href="">Шашлыки</a></li>
                                    <li>/</li>
                                    <li><a href="">Ассорти</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="product-card-bg">
                                <div class="row">
                                    <div class="col-sm-5 wow fadeInLeft">
                                        <div class="product-img">
                                            <img src="images/img1.jpg" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="col-sm-7 wow fadeInRight">
                                        <div class="product-title">
                                            <h5>Ханское ассорти</h5>
                                        </div>
                                        <div class="product-price">
                                            <p>20 000 т</p>
                                        </div>
                                        <div class="count">
                                            <label for="">Количество</label>
                                            <div class="count-input">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                                                <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                                            </div>
                                        </div>
                                        <div class="row product-row">
                                            <div class="col">
                                                <div class="order-dark-btn">
                                                    <button>Заказать <img src="images/basket-ico.svg" alt=""></button>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="favorite-btn">
                                                    <button><img src="images/heart.png" alt="">В избранное</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 wow fadeInUp">
                                        <div class="product-text space-top-sm">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem...</p>
                                        </div>

                                        <div class="collapse" id="demo">
                                            <div class="product-text">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem...</p>
                                            </div>
                                        </div>
                                        <div class="show-more">
                                            <p href="#demo" data-toggle="collapse" data-target="#demo">Подробнее <img src="images/arrow.png" alt=""></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-sm-12">
							<p class="catalog-item__title">Рекомендуемые товары</p>
						</div>
                         <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="promo">
                                        <img src="images/new-label.png" alt="">
                                    </div>
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <div class="promo">
                                        <img src="images/promo-label.png" alt="">
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

