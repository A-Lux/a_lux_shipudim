<?php require_once 'inner-header.php'?>
<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow slideInLeft">
                                <ul>
                                    <li><a href="">Главная</a></li>
                                    <li>/</li>
                                    <li><a href="">Каталог</a></li>
                                </ul>
                            </div>
                            <!-- <div class="inner-menu wow fadeInUp">
                                <div class="inner-title">
                                    <h3>Меню</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 space-top-sm border-right-strip">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo">
                                                        Шашлык <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo2">
                                                        Завтраки
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo2">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo3">
                                                        Детское меню
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo3">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo4">
                                                        Салаты
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo4">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top-sm border-right-strip">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo5">
                                                        Закуски <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo5">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo6">
                                                        Горячие блюда
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo6">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo7">
                                                        Хлеб
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo7">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo8">
                                                        Десерты
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo8">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top-sm ">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo9">
                                                        Напитки <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo9">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo10">
                                                        Жевательная резинка
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo10">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo3">
                                                        Бой посуды
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo3">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="" data-toggle="collapse" data-target="#demo4">
                                                        Салаты
                                                        <img src="images/arrow.png" alt="">
                                                    </a>
                                                    <div class="sublist collapse" id="demo4">
                                                        <ul>
                                                            <li><a href="">Ассорти «Shipudim»</a></li>
                                                            <li><a href="">Соусы</a></li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top">
                                        <div class="promo">
                                            <a href=""><img src="images/discount.png" alt="">Акции</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top">
                                        <div class="promo">
                                            <a href=""><img src="images/new.png" alt="">Новинки</a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="promo">
                                        <img src="images/new-label.png" alt="">
                                    </div>
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <div class="promo">
                                        <img src="images/promo-label.png" alt="">
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                       <div class="quantity">
                                           <input type="number" value="1" min="1">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                        <div class="quantity">
                                            <input type="number" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="promo">
                                        <img src="images/new-label.png" alt="">
                                    </div>
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                        <div class="quantity">
                                            <input type="number" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                        <div class="quantity">
                                            <input type="number" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 wow rollIn">
                            <div class="catalog-item">
                                <div class="image">
                                    <div class="like">
                                        <a href=""><img src="images/like.png" alt=""></a>
                                    </div>
                                    <div class="promo">
                                        <img src="images/promo-label.png" alt="">
                                    </div>
                                    <img src="images/img5.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="text-block">
                                    <h6>Ханское ассорти</h6>
                                    <p>Люля- кебаб из баранины, баклажан-
                                        кебаб, утка, курица, цыпленок...</p>
                                    <h6>20 000 тг</h6>
                                    <div class="order-block ">
                                        <button>Заказать<img src="images/basket-ico.svg" alt=""></button>
                                        <div class="quantity">
                                            <input type="number" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="pagination wow fadeInUp">
                                <ul>
                                    <li><a href="">1</a></li>
                                    <li><a href="">2</a></li>
                                    <li class="active"><a href="">3</a></li>
                                    <li><a href="">4</a></li>
                                    <li><a href="">»</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'footer.php'?>
