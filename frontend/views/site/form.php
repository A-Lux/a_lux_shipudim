<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Shipudim</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body>
    <!--<div class="before-load">-->
    <!--    <div class="video-overlay">-->
    <!--    </div>-->
    <!--    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">-->
    <!--        <source src="/images/video.mp4" type="video/mp4">-->
    <!--    </video>-->
    <!--    <img src="images/logo.svg" alt="">-->
    <!--</div>-->

    <content class="mob-view">
        <input id="hamburger" class="hamburger" type="checkbox">
        <label class="hamburger" for="hamburger">
            <i></i>
            <text style="display:none;">
                <close>закрыть</close>
                <open>меню</open>
            </text>
        </label>
        <section class="drawer-list">

            <ul>
                <li>
                    <div class="phone mobile-version">
                        <a href="tel:+7 777 470 62 41"><img src="images/phone-ico.svg" alt="">+7 777 470 62 41</a>
                    </div>
                </li>
                <li><a data-id="1" href=""> Меню ресторана</a></li>
                <li><a data-id="2" href="">Акции и новинки</a></li>
                <li><a data-id="3" href="">Наши услуги </a></li>
                <li><a data-id="4" href="">Вакансии </a></li>
                <li><a data-id="5" href="">Сотрудничество</a></li>
                <li>
                    <a data-id="6" href="catalog.php">Каталог</a>
                </li>
                <li>
                    <a data-id="7" href="product-card.php">Карточка товара</a>
                </li>
                <li>
                    <a data-id="8" href="../card/basket.php">Корзина </a>
                </li>
                <li>
                    <a data-id="9" href="../content/vacancy.php">Вакансии</a>
                </li>
                <li>
                    <a data-id="10" href="cabinet.php">Личный кабинет</a>
                </li>
            </ul>
        </section>
    </content>
    <div class="content">
        <header class="header">

            <div class="menu-bg">
                <div class="container wow fadeInDown">
                    <div class="row hide-load">
                        <div class="col-sm-1">
                            <div class="logo">
                                <a href="index.php"><img src="images/logo.png" alt=""></a>
                            </div>
                        </div>

                        <div class="col-sm-8 col-md-7">
                            <div class="menu-row ">
                                <div class="menu">
                                    <!-- <img src="images/pod.svg" alt=""> -->

                                    <div class="toggle-menu" style="display: block;">
                                        <ul>
                                            <div class="dropdown">
                                                <button class="btn dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <p>Меню доставки</p>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="#">Шашлыки</a>
                                                    <a class="dropdown-item" href="#">ГРУЗИНСКАЯ КУХНЯ</a>
                                                    <a class="dropdown-item" href="#">САЛАТЫ</a>
                                                    <a class="dropdown-item" href="#">ЗАКУСКИ/ХЛЕБ</a>
                                                    <a class="dropdown-item" href="#">СУПЫ</a>
                                                    <a class="dropdown-item" href="#">ГОРЯЧИЕ БЛЮДА</a>
                                                    <a class="dropdown-item" href="#">ПИЦЦА</a>
                                                    <a class="dropdown-item" href="#">СОУСЫ</a>
                                                    <a class="dropdown-item" href="#">НАПИТКИ</a>
                                                    <a class="dropdown-item" href="#">ВЕСОВОЙ ШАШЛЫК</a>
                                                </div>
                                            </div>
                                            <li><a href="">Акции и новинки</a></li>
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    О нас
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="#">О КОМПАНИИ</a>
                                                    <a class="dropdown-item" href="#">ВАКАНСИИ</a>
                                                    <a class="dropdown-item" href="#">ПАРТНЕРАМ</a>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                    <!-- <div class="menu-cover is-active" id="close-menu">
                                        <div class="hamburger"></div>
                                    </div> -->
                                </div>

                                <div class="search">
                                    <input type="text" name="search" autocomplete="off" onfocus="this.value=''">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2 col-md-3">
                            <div class="phone desktop-version">
                                <a href="tel:+7 777 470 62 41"><img src="images/phone-ico.svg" alt="">+7 777 470 62
                                    41</a>
                            </div>
                        </div>
                        <!-- <div class="col-sm">
                        <div class="lang">
                            <img src="images/world-ico.svg" alt="">
                            <select name="" id="">
                                <option value="">RU</option>
                                <option value="">KZ</option>
                            </select>
                        </div>

                    </div> -->

                    </div>
                </div>
            </div>
            <div class="container form-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Оформление заказа</h2>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-parent">
                            <p>Для новых покупателей</p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>Контактное лицо (ФИО):</p>
                                    <input type="text">
                                </div>
                                <div class="col-sm-6">
                                    <p>Контактный телефон:</p>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>E-mail:</p>
                                    <input type="text">
                                </div>
                                <div class="col-sm-12">
                                    <p>Адресс доставки:</p>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-wrap">
                                        <p>Способ доставки:</p>
                                        <input type="radio" name="receive" id="">
                                        <p>Курьер</p>
                                        <input type="radio" name="receive" id="">
                                        <p>Самовывоз</p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-wrap">
                                        <p>Способ оплаты:</p>
                                        <input type="radio" name="payment" id="">
                                        <p>Наличный расчёт</p>
                                        <input type="radio" name="payment" id="">
                                        <p>безналичный курьеру</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <a href="">
                                        Подтвердить заявку
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                            <div class="form-parent">
                                <p>Для постоянных покупателей</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>E-mail:</p>
                                        <input type="text">
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Дата рождения:</p>
                                        <input type="text">
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="">войти</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                
            </div>
    </div>
    </header>


    <footer class="footer">
        <div class="menu-bg">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-sm-1">
                        <a href=""><img src="images/logo.png" alt=""></a>
                    </div>
                    <div class="col-sm-9 col-md-7 col-lg-9 ">
                        <!-- <div class="menu">
                        <img src="images/pod.svg" alt="">
                        <p>Меню доставки</p>
                    </div> -->
                        <div class="menu">
                            <!-- <img src="images/pod.svg" alt=""> -->
                            <!-- <p>Меню доставки</p> -->
                            <div class="toggle-menu" style="display: block;">
                                <ul>
                                    <li><a href="">Наши услуги </a></li>
                                    <li><a href="">Вакансии </a></li>
                                    <li><a href="">Сотрудничество</a></li>
                                </ul>
                            </div>
                            <!-- <div class="menu-cover" id="close-menu">
                                <div class="hamburger"></div>
                            </div> -->
                        </div>
                        <div class="partners desktop-version">
                            <p>Наши партнеры <a href=""><img src="images/partner1.jpg" alt=""></a><a href=""><img
                                        src="images/partner2.jpg" alt=""></a></p>
                        </div>
                        <div class="partners mobile-version">
                            <p>Наши партнеры </p>
                            <a href=""><img src="images/partner1.jpg" alt=""></a><a href=""><img
                                    src="images/partner2.jpg" alt=""></a>
                        </div>

                    </div>


                    <div class="col-sm-2 col-md-4 col-lg-2">
                        <div class="input-block">
                            <input type="text" placeholder="+80123456789">
                            <button>Оставить заявку</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </footer>

</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>


</html>