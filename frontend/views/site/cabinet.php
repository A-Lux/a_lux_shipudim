<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow fadeInLeft">
                                <ul>
                                    <li><a href="">Главная</a></li>
                                    <li>/</li>
                                    <li><a href="">Личный кабинет</a></li>
                                </ul>
                            </div>
                            <div class="inner-title">
                                <h3>Личный кабинет</h3>
                            </div>
                        </div>
                        <div class="col-sm-3 wow fadeInLeft space-top-sm border-right-strip">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Мои заказы</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Избранное</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Мои данные</a>
                                </div>
                            </nav>
                        </div>
                        <div class="col-sm-9 wow fadeInRight space-top-sm">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="tab-title">
                                        <h6>Мои данные</h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="cabinet-input">
                                                <input type="text" placeholder="Фамилия">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="cabinet-input">
                                                <input type="text" placeholder="Имя">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="cabinet-input">
                                                <input type="text" placeholder="Отчество">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="cabinet-input">
                                                <label for="">Ваш E-mail</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="cabinet-input">
                                                <input type="text" placeholder="E-mail">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <label for="">Пароль</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <input type="password" placeholder="Изменить">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <label for="">Телефон</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <input type="text" placeholder="+7 (___) ___-__-__">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <label for="">Адрес</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <input type="text" placeholder="Ваш адрес">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <label for="">Пароль</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="cabinet-input">
                                                        <input type="text" placeholder="Изменить">
                                                    </div>
                                                </div>
                                                <div class="offset-lg-6 col-sm-6">
                                                        <div class="save-btn">
                                                            <button href="">Сохранить</button>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">2...</div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">3...</div>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
