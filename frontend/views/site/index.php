<div class="content">
    <div class="main">
        <div class="main-food-bg">
            <div class="section2">
                <div class="row">
                    <div class="container">
                        <div class="glide">
                            <div class="glide__track" data-glide-el="track">
                                <ul class="glide__slides">
                                    <?foreach($slider as $value):?>
                                    <li class="glide__slide">
                                        <div class="pros-card">
                                            <h2><?=$value->title;?></h2>
                                            <?=$value->content;?>
                                        </div>
                                    </li>
                                    <?endforeach;?>
                                </ul>
                            </div>
                            <div class="glide__arrows" data-glide-el="controls">
                                <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                                    <img src="/images/prev.svg" alt="">
                                </button>
                                <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                                    <img src="/images/next.svg" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section3" id="section3">
                <div class="row">
                    <div class="grid-box">
                        <?foreach($catalog as $value):?>
                        <a href="<?=\yii\helpers\Url::to(['/catalog/index', 'url' => $value->url])?>">
                            <div class="grid-item">
                                <div class="absolute-gradient"></div>
                                <img src="<?=$value->getImage();?>" alt="">
                                <p><?=$value->name;?></p>
                            </div>
                        </a>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
            <div class="section4">
                <div class="container">
                    <div class="row wow fadeInUp">
                        <div class="col-sm-12">
                            <div class="main-title">
                                <h5><?=$mainSub[1]->name;?></h5>
                            </div>
                            <div class="map">
                                <div id="map" style="width: 100%; height: 500px;border:0"></div>
                                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"
                                    type="text/javascript"></script>
                                <script>
                                    //document.getElementById("office-city").innerHTML="<?//=$city->name?>//";
                                    ymaps.ready(init);
                                    var center_map = [0, 0];
                                    var map = "";

                                    function init() {
                                        map = new ymaps.Map('map', {
                                            center: [43.238293, 76.945465],
                                            zoom: 12,
                                        });
                                        map.behaviors.disable('scrollZoom');
                                        var myGeocoder = ymaps.geocode("Алматы");
                                        myGeocoder.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var coords = street.geometry.getCoordinates();
                                                map.setCenter(coords);
                                            },
                                            function (err) {

                                            }
                                        );

                                            <? foreach($address as $v) :?>
                                            map.geoObjects.add(new ymaps.Placemark([<?= $v -> latitude ?>, <?= $v -> longitude ?>], {
                                                balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->phone?>, <?=$v->mobile_phone;?></div>'
                                            }, {
                                                    iconLayout: 'default#image',
                                                    iconImageHref: '/images/loc-ico.svg',
                                                    preset: 'islands#icon',
                                                    iconColor: '#0095b6'
                                                }));
                                            <? endforeach;?>
                                        }
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="line-bg wow fadeInUp">
                    <div class="container">
                        <div class="owl-wrap">
                            <div class="owl-carousel owl-theme">
                                <?foreach($address as $value):?>
                                <div class="item">
                                    <!-- <div class="circle">
                                                <?=$value->sort;?>
                                            </div> -->
                                    <div class="loc">
                                        <div class="icon">
                                            <img src="/images/loc-ico.svg" alt="">
                                        </div>
                                        <p><?=$value->address;?></p>
                                    </div>
                                    <div class="car-sep"></div>
                                    <div class="clock">
                                        <div class="icon">
                                            <img src="/images/clock-ico.svg" alt="">
                                        </div>
                                        <p><?=$value->content;?></p>
                                    </div>
                                    <div class="phone">
                                        <div class="icon">
                                            <img src="/images/phone-ico-owl.svg" alt="">
                                        </div>
                                        <p><?=$value->phone?>, <?=$value->mobile_phone;?></p>
                                    </div>
                                </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-btn wow tada">
                    <a onclick="topFunction()">
                        <img src="/images/up.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
</script>