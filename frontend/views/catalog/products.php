<div class="row tablet-version-hide items">
    <div class="col-sm-12">
        <div class="bread-crumbs wow slideInLeft">
            <ul>
                <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                <li>/</li>
                <li><a href="<?=$menu[4]->url;?>"><?=$menu[4]->text;?></a></li>
            </ul>
        </div>
    </div>


        <?foreach($products as $v):?>
            <div class="col-sm-3 wow rollIn">
                <div class="catalog-item">
                    <div class="image">
                        <div class="like">
                            <a href=""><img src="/images/like.png" alt=""></a>
                        </div>
                        <img src="<?=$v->getImage();?>" alt="" class="img-fluid">
                    </div>
                    <div class="text-block">
                        <h6><?=$v->name;?></h6>
                        <p><?=$v->content;?></p>
                        <h6><?=$v->price;?> тг</h6>
                        <div class="order-block ">
                            <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                Заказать
                                <img src="/images/basket-ico.svg" alt="">
                            </button>
                            <div class="quantity">
                                <input type="number" value="1" min="1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach;?>

    <div class="col-sm-6">
        <!--                            <div class="pagination wow fadeInUp">-->
        <!--                                <ul>-->
        <!--                                    <li><a href="">1</a></li>-->
        <!--                                    <li><a href="">2</a></li>-->
        <!--                                    <li class="active"><a href="">3</a></li>-->
        <!--                                    <li><a href="">4</a></li>-->
        <!--                                    <li><a href="">»</a></li>-->
        <!--                                </ul>-->
        <!--                            </div>-->
    </div>
    <div class="col-sm-6">
        <div class="inner-up-btn wow tada">
            <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
        </div>
    </div>
</div>