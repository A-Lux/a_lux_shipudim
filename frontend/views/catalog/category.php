<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row tablet-version-hide items">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow slideInLeft">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="<?=$menu[4]->url;?>"><?=$menu[4]->text;?></a></li>
                                </ul>
                            </div>

                            <!-- <div class="inner-menu wow fadeInUp">
                                <div class="inner-title">
                                    <h3>Меню</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 space-top-sm border-right-strip">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <?$m = 0;?>
                                                <?foreach($catalog as $value):?>
                                                    <? $m++; ?>
                                                    <? if ($m < 5): ?>
                                                        <li>
                                                            <a href="" data-toggle="collapse" data-id="<?=$value->id?>"
                                                               class="catalog_filter">
                                                                <?=$value->name;?> <img src="/images/arrow.png" alt="">
                                                            </a>
                                                        </li>
                                                    <?endif;?>
                                                <?endforeach;?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top-sm border-right-strip">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <?$m = 0;?>
                                                <?foreach($catalog as $value):?>
                                                    <? $m++; ?>
                                                    <? if ($m > 4 && $m < 9): ?>
                                                        <li>
                                                            <a href="" data-toggle="collapse" data-id="<?=$value->id?>"
                                                               class="catalog_filter">
                                                                <?=$value->name;?> <img src="/images/arrow.png" alt="">
                                                            </a>
                                                        </li>
                                                    <?endif;?>
                                                <?endforeach;?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top-sm ">
                                        <div class="inner-menu-list">
                                            <ul>
                                                <?$m = 0;?>
                                                <?foreach($catalog as $value):?>
                                                    <? $m++; ?>
                                                    <? if ($m > 8): ?>
                                                        <li>
                                                            <a href="" data-toggle="collapse" data-id="<?=$value->id?>"
                                                               class="catalog_filter">
                                                                <?=$value->name;?> <img src="/images/arrow.png" alt="">
                                                            </a>
                                                        </li>
                                                    <?endif;?>
                                                <?endforeach;?>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 space-top">
                                        <div class="promo">
                                            <a href=""><img src="/images/discount.png" alt="">Акции</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 space-top">
                                        <div class="promo">
                                            <a href=""><img src="/images/new.png" alt="">Новинки</a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>

                        <?foreach($catalogs as $value):?>
                            <?$products = $value->products;?>
                            <?foreach($products as $v):?>
                                <div class="col-sm-3 wow rollIn">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <div class="like btn-add-favorite" data-id="<?=$value->id;?>"
                                                 data-user-id="<?=Yii::$app->user->id?>">
                                                <a href=""><img src="/images/like.png" alt=""></a>
                                            </div>
                                            <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'id' => $v->id])?>">
                                                <img src="<?=$v->getImage();?>" alt="" class="img-fluid"></a>
                                        </div>
                                        <div class="text-block">
                                            <h6><?=$v->name;?></h6>
                                            <p><?=$v->content;?></p>
                                            <h6><?=$v->price;?> тг</h6>
                                            <div class="order-block ">
                                                <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                                    Заказать
                                                    <img src="/images/basket-ico.svg" alt="">
                                                </button>
                                                <div class="quantity">
                                                    <input type="number" min="1" name="count" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        <?endforeach;?>

                        <div class="col-sm-6">
                            <!--                            <div class="pagination wow fadeInUp">-->
                            <!--                                <ul>-->
                            <!--                                    <li><a href="">1</a></li>-->
                            <!--                                    <li><a href="">2</a></li>-->
                            <!--                                    <li class="active"><a href="">3</a></li>-->
                            <!--                                    <li><a href="">4</a></li>-->
                            <!--                                    <li><a href="">»</a></li>-->
                            <!--                                </ul>-->
                            <!--                            </div>-->
                        </div>
                        <div class="col-sm-6">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
