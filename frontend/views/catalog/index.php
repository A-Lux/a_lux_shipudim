<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row tablet-version-hide items">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow slideInLeft">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href=""><?=$menu[4]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="/"><?=$catalog->name;?></a></li>
                                </ul>
                            </div>
                        </div>

                            <?foreach($products['data'] as $v):?>
                                <div class="col-sm-3 wow rollIn">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <div class="like">
                                                <a class="btn-add-favorite" data-id="<?=$v->id;?>"
                                                   data-user-id="<?=Yii::$app->user->id?>">
                                                    <img src="/images/like.png" alt=""></a>
                                            </div>
                                            <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'url' => $v->url])?>">
                                            <img src="<?=$v->getImage();?>" alt="" class="img-fluid"></a>
                                        </div>
                                        <div class="text-block">
                                            <h6><?=$v->name;?></h6>
                                            <p><?=$v->content;?></p>
                                            <h6><?=$v->price;?> тг</h6>
                                            <div class="order-block ">
                                                <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                                    Заказать
                                                    <img src="/images/basket-ico.svg" alt="">
                                                </button>
                                               <div class="quantity">
                                                   <input class="count<?=$v->id;?>" type="number" min="1" name="count"  value="1">
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        <div class="col-sm-12">
                            <div class="pagination">
                                <ul>
                                    <?=\yii\widgets\LinkPager::widget([
                                        'pagination' => $products['pagination'],
                                        'activePageCssClass' => 'active',
                                        'hideOnSinglePage' => true,
                                        'prevPageLabel' => '<',
//                                        'prevPageCssClass' => ['class' => 'fas fa-chevron-left'],
                                        'nextPageLabel' => '>',
//                                        'nextPageCssClass' => ['class' => 'fas fa-chevron-right'],
                                    ]);
                                    ?>
                                </ul>
                            </div>
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
