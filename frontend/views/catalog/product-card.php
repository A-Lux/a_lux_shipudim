<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs">
                                <ul>
                                    <li><a href="<?= $menu[0]->url; ?>"><?= $menu[0]->text; ?></a></li>
                                    <li>/</li>
                                    <li><a href=""><?= $menu[4]->text; ?></a></li>
                                    <li>/</li>
                                    <li><a href="/catalog/<?= $product->category->url; ?>"><?= $product->category->name; ?></a></li>
                                    <li>/</li>
                                    <li><a href=""><?= $product->name; ?></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="product-card-bg">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="product-img">
                                            <img src="<?= $product->getImage(); ?>" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="product-title">
                                            <h5><?= $product->name; ?></h5>
                                        </div>
                                        <div class="product-price">
                                            <p><?= $product->price; ?> т</p>
                                        </div>
                                        <div class="count">
                                            <label for="">Количество</label>
                                            <div class="count-input">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                                      class="btn-operation">-</span>
                                                <input class="quantity input-text" min="1" name="count" value="1"
                                                       type="number">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                                      class="btn-operation">+</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="order-dark-btn">
                                                    <button class="btn-to-basket" type="button" name="button" data-id="<?=$product->id;?>">
                                                        Заказать <img src="/images/basket-ico.svg" alt="">
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="favorite-btn">
                                                    <button class="btn-add-favorite" data-id="<?=$product->id;?>" data-user-id="<?=Yii::$app->user->id?>">
                                                        <img src="/images/heart.png" alt="">В избранное</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="product-text space-top-sm">
                                            <p><?=$product->content;?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="inner-up-btn">
                                <a onclick="topFunction()">Наверх<img src="images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
