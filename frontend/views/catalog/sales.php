<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="<?=$menu[7]->url;?>"><?=$menu[7]->text;?></a></li>
                                </ul>
                            </div>
                            <div class="inner-title">
                                <h3><?=$menu[7]->text;?></h3>
                            </div>
                        </div>
                        <?foreach ($product['data'] as $value): ?>
                            <div class="col-sm-3">
                                    <div class="sales_item">
                                        <div class="col-sm-12 wow fadeInRight sales px-0">
                                            <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'url' => $value->url])?>">
                                                <img src="<?=$value->getImage();?>" alt="">
                                            </a>
                                            <div class="row product-row">
                                                <p><?=$value->name;?> </p>
                                                 <br>
                                                <p>:   <?=$value->price?> тг</p>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        <?endforeach;?>
                        <div class="col-sm-12">
                            <div class="pagination">
                                <ul>
                                    <?=\yii\widgets\LinkPager::widget([
                                        'pagination' => $product['pagination'],
                                        'activePageCssClass' => 'active',
                                        'hideOnSinglePage' => true,
                                        'prevPageLabel' => '<',
//                                        'prevPageCssClass' => ['class' => 'fas fa-chevron-left'],
                                        'nextPageLabel' => '>',
//                                        'nextPageCssClass' => ['class' => 'fas fa-chevron-right'],
                                    ]);
                                    ?>
                                </ul>
                            </div>
                            <div class="inner-up-btn">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>