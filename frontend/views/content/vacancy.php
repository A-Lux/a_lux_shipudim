<?php

use yii\widgets\LinkPager;
use yii\widgets\Pjax;

?>
<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow fadeInLeft">
                                <ul>
                                    <li><a href="<?= $menu[0]->url; ?>"><?= $menu[0]->text; ?></a></li>
                                    <li>/</li>
                                    <li><a href="<?= $menu[6]->url; ?>"><?= $menu[6]->text; ?></a></li>
                                </ul>
                            </div>
                            <div class="inner-title">
                                <h3><?= $menu[6]->text; ?></h3>
                            </div>
                        </div>
                        <? $m = 0; ?>
                        <? foreach ($vacancy['data'] as $value): ?>

                            <div class="modal fade" id="vacancyModal<?= $value->id ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="vacancyModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content modal-login modal-personal">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="vacancyModalLabel"><?= $value->title ?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="">
                                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                                                   value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                                            <input type="hidden" name="Respond[vacancy_id]" value="<?= $value->id ?>">
                                            <div class="modal-body">
                                                <div class="modal-text">
                                                    <p></p>
                                                </div>
                                                <div class="authorization-input">
                                                    <input placeholder="ФИО" name="Respond[name]"
                                                           value="<?= $userProfile->fio; ?>">
                                                </div>
                                                <div class="authorization-input">
                                                    <input placeholder="Телефон" name="Respond[phone]"
                                                           value="<?= Yii::$app->user->identity->username; ?>">
                                                </div>
                                                <div class="authorization-input">
                                                    <textarea placeholder="Раскажите о себе"
                                                              name="Respond[text]"></textarea>
                                                </div>
                                                <div class="authorization-input">
                                                    <div class="attach-file shipudim-form">
                                                        <a data-inputid="fileInput<?= $value->id ?>" href=""><img
                                                                    data-inputid="fileInput<?= $value->id ?>"
                                                                    src="/images/attach-icon.png" alt=""> Прикрепить
                                                            резюме
                                                        </a><br>
                                                    </div>
                                                    <div class="modal-files file-form">

                                                    </div>

                                                    <input type="file" class="uploadImgInputForm"
                                                           id="fileInput<?= $value->id ?>" style="display: none;">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn respond-vacancy-btn"
                                                        data-vacancy-id="<?= $value->id ?>">Отправить
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <? $m++; ?>
                            <? if ($m % 3 == 1 || $m % 4 == 2): ?>
                                <div class="space-top col-sm-6 wow fadeIn border-right-strip">
                                    <div class="vacancy-item">
                                        <a href="">
                                            <h6><?= $value->title; ?></h6>
                                            <div class="salary">
                                                <p><span><?= $value->salary; ?></span>/ <?= $value->payment; ?></p>
                                            </div>
                                            <p><?= $value->time; ?></p>
                                        </a>
                                        <p>
                                            <?=$value->content;?>
                                        </p>
                                    </div>

                                    <a class="vacancySubmit" data-toggle="modal"
                                       data-target="#vacancyModal<?= $value->id ?>">
                                        Откликнуться
                                    </a>
                                </div>
                            <? endif; ?>

                            <? if ($m % 3 == 0): ?>
                                <div class="space-top col-sm-6 wow fadeIn border-right-strip">
                                    <div class="vacancy-item">
                                        <a href="">
                                            <h6><?= $value->title; ?></h6>
                                            <div class="salary">
                                                <p><span><?= $value->salary; ?></span>/ <?= $value->payment; ?></p>
                                            </div>
                                            <p><?= $value->time; ?></p>
                                        </a>
                                        <p>
                                            <?=$value->content;?>
                                        </p>
                                    </div>

                                    <a class="vacancySubmit" data-toggle="modal"
                                       data-target="#vacancyModal<?= $value->id ?>">
                                        Откликнуться
                                    </a>

                                </div>
                            <? endif; ?>

                        <? endforeach; ?>

                        <div class="col-sm-12">
                            <div class="pagination wow fadeInUp">
                                <ul>
                                    <?= LinkPager::widget([
                                        'pagination' => $vacancy['pagination'],
                                        'activePageCssClass' => 'active',
                                        'hideOnSinglePage' => true,
                                        'prevPageLabel' => '',
                                        'prevPageCssClass' => ['class' => 'prev-page'],
                                        'nextPageLabel' => '&raquo;',
                                        'nextPageCssClass' => ['class' => 'next-page'],
                                        // Настройки контейнера пагинации
                                        'options' => [
                                            'tag' => 'ul',
                                            'class' => 'pagination',
                                        ],

                                        // Настройки классов css для ссылок

                                    ]);
                                    ?>
                                </ul>
                            </div>
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
