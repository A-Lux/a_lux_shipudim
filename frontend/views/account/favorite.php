<div class="favorites">
    <div class="tab-title">
        <? if ($fav == null): ?>
            Ваша избранное пуста!
        <? endif; ?>
    </div>
    <? if ($fav != null): ?>
        <div class="row" style="width: 100%;">
            <?foreach($fav as $v):?>
                <div class="col-sm-4 wow rollIn">
                    <div class="catalog-item">
                        <div class="image">
                            <div class="like">
                                <div class="delete-icon btn-delete-product-from-favorite"
                                     data-id="<?= $v->id; ?>">
                                    &#10005
                                </div>
                            </div>
                            <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'id' => $v->id])?>">
                                <img src="<?=$v->getImage();?>" alt="" class="img-fluid"></a>
                        </div>
                        <div class="text-block">
                            <h6><?=$v->name;?></h6>
                            <p><?=$v->content;?></p>
                            <h6><?=$v->price;?> тг</h6>
                            <div class="order-block ">
                                <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                    Заказать
                                    <img src="/images/basket-ico.svg" alt="">
                                </button>
                                <div class="quantity">
                                    <input class="count<?=$v->id;?>" type="number" min="1" name="count"  value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    <?endif;?>
</div>