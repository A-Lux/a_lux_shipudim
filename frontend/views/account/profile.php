<?php

use yii\widgets\LinkPager;

?>
<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow fadeInLeft">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="<?=$menu[9]->url;?>"><?=$menu[9]->text;?></a></li>
                                </ul>
                            </div>
                            <div class="inner-title">
                                <h3><?=$menu[9]->text;?></h3>
                            </div>
                        </div>
                        <div class="col-sm-3 wow fadeInLeft space-top-sm border-right-strip">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Мои данные</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Избранное</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Мои заказы</a>
                                </div>
                            </nav>
                        </div>
                        <div class="col-sm-9 wow fadeInRight space-top-sm">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="tab-title">
                                        <h6>Мои данные</h6>
                                    </div>
                                    <form action="">
                                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                                               value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="cabinet-input">
                                                    <input type="text" placeholder="Фамилия" value="<?=$profile->surname;?>" name="UserProfile[surname]">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cabinet-input">
                                                    <input type="text" placeholder="Имя" value="<?=$profile->name;?>" name="UserProfile[name]">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cabinet-input">
                                                    <input type="text" placeholder="Отчество" value="<?=$profile->father;?>" name="UserProfile[father]">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cabinet-input">
                                                    <label for="">Ваш E-mail</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cabinet-input">
                                                    <input type="text" placeholder="E-mail" value="<?=$user->email;?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <label for="">Телефон</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <input type="text" placeholder="+7 (___) ___-__-__" value="<?=$user->username;?>"
                                                                   name="User[username]">
                                                            <script>
                                                                $('input[name="User[username]"]').inputmask("8(999) 999-9999");
                                                            </script>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <label for="">Адрес</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <input type="text" name="UserProfile[address]" placeholder="Ваш адрес"
                                                                   value="<?= $profile->address; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <label for="">Пароль</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <input type="password" placeholder="Пароль" value="<?=$user->password;?>"
                                                                   name="User[password]">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <label for="">Повторите пароль</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="cabinet-input">
                                                            <input type="password" placeholder="Повторить пароль" value="<?=$user->password_verify;?>"
                                                                   name="User[password_verify]">
                                                        </div>
                                                    </div>
                                                    <div class="offset-lg-6 col-sm-6">
                                                        <div class="save-btn">
                                                            <button type="button" class="update-profile-button">Сохранить</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="favorites">
                                        <div class="tab-title">
                                            <? if ($fav == null): ?>
                                                <p>Вы еще не добавили ничего в избранное.</p>
                                                <p>Что бы добавить понравившееся блюдо в избранное,кликните на  желтое
                                                    сердечко на нужном товаре.</p>
                                            <? endif; ?>
                                        </div>
                                        <? if ($fav != null): ?>
                                            <div class="row" style="width: 100%;">
                                                <?foreach($fav as $v):?>
                                                <div class="col-sm-4 wow rollIn">
                                                    <div class="catalog-item">
                                                        <div class="image">
                                                            <div class="like">
                                                                <div class="delete-icon btn-delete-product-from-favorite"
                                                                     data-id="<?= $v->id; ?>">
                                                                    &#10005
                                                                </div>
                                                            </div>
                                                            <a href="<?=\yii\helpers\Url::to(['/catalog/product-card', 'id' => $v->id])?>">
                                                                <img src="<?=$v->getImage();?>" alt="" class="img-fluid"></a>
                                                        </div>
                                                        <div class="text-block">
                                                            <h6><?=$v->name;?></h6>
                                                            <p><?=$v->content;?></p>
                                                            <h6><?=$v->price;?> тг</h6>
                                                            <div class="order-block ">
                                                                <button class="btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                                                    Заказать
                                                                    <img src="/images/basket-ico.svg" alt="">
                                                                </button>
                                                                <div class="quantity">
                                                                    <input class="count<?=$v->id;?>" type="number" min="1" name="count"  value="1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endforeach;?>
                                            </div>
                                        <?endif;?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <?if($isOrder != null):?>
                                    <table class="table table-responsive">
                                        <tr>
                                            <th scope="col">Номер заказа</th>
                                            <th scope="col">Дата оформление</th>
                                            <th scope="col">Статус доставки</th>
                                            <th scope="col">Адрес доставки</th>
                                            <th scope="col">Сумма</th>
                                        </tr>

                                        <? foreach ($orders['data'] as $v): ?>
                                            <tr>
                                                <th scope="row"><?= $v->product_id; ?></th>
                                                <td><?= $v->created_at; ?></td>
                                                <td><?= $v->address;?></td>
                                                <td><?= $v->sum; ?> тг</td>
                                            </tr>
                                        <? endforeach; ?>
                                    </table>
                                    <div class="pagination">
                                        <ul>
                                            <?= LinkPager::widget([
                                                'pagination' => $orders['pagination'],
                                                'activePageCssClass' => 'active',
                                                'hideOnSinglePage' => true,
                                                'prevPageLabel' => '<',
                                                'nextPageLabel' => '>',
                                            ]);
                                            ?>

                                        </ul>
                                    </div>
                                    <?else:?>
                                        <div class="tab-title">
                                            У Вас еще нет заказов!
                                        </div>
                                    <?endif;?>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
