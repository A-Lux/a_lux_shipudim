<section class="cart">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <ul class="breadcrumbs">
                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                    <li>/</li>
                    <li><a href="<?=$menu[8]->url;?>"><?=$menu[8]->text?></a></li>
                </ul>
            </div>
        </div> <!-- End row -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="cart-title__wrapper">
                    <p class="cart-title">Оформление заказа</p>
                    <span class="cart-title__line"></span>
                </div>
            </div>
        </div> <!-- End row -->
        <div class="row" style="margin-top: 1rem;">
            <div class="col-lg-6 col-md-12 col-sm-12" style="border-right: 1px dashed silver;">
                <form action="">
                    <input type="hidden" class="order_id" value="<?=$product_id?>">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__name-title">
                                <p>Заполните контактные данные</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__input-name_wrapper">
                                <div class="form-group">
                                    <label for="nameInput">Контактное лицо (ФИО): <span
                                            class="cart-left__input-name_star">*</span></label>
                                    <input type="text" class="form-control name-order" id="nameInput" name="name_and_surname"
                                           placeholder="ФИО" value="<?= !empty($profile->fio) ? $profile->fio : '';?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="telInput">Контактный телефон: <span
                                            class="cart-left__input-name_star">*</span></label>
                                    <input type="text" class="form-control phone" id="telInput" name="phone_username"
                                           placeholder="+7(___)-___-__-__" value="<?=$user->username?>" required>
                                    <script>
                                        $('input[name="phone_username"]').inputmask("8(999) 999-9999");
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__delivery-title">
                                <p>Доставка</p>
                                <span>Способ доставки:</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__delivery">
                                <label for="check1">
                                        <span class="checkSpan active" id="checkSpanCourier" data-tab="1">

                                        </span>
                                    <input type="checkbox" id="check1" class="check check1" data-role="courier" name="courier">
                                </label>
                                <p>Курьером</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__delivery_text">
                                <?=$info[0]->text ? $info[0]->text : '';?>
                            </div>
                        </div>
                    </div>
                    <div id="box-courier">
                        <div class="row" id="address">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cart-left__input-name_wrapper">
                                    <?=$this->render('delivery_zones', compact('geocoords', 'sum'));?>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="typePay-1">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cart-left__delivery-title">
                                    <p>Оплата</p>
                                    <span>Способ оплаты:</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="typePay-2">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-left_wrapper-wallet">
                                    <ul>
                                        <li><label for="check2">
                                                   <span class="checkSpan2" id="checkSpanCard" data-tab="0">

                                                    </span>
                                                <input type="checkbox" id="check2" class="check check2" data-wallet="card">
                                            </label></li>
                                        <li>
                                            <p>Банковская карта курьеру - <br> Pos Терминал </p>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li><label for="check3">
                                                    <span class="checkSpan2" id="checkSpanCash" data-tab="0">

                                                    </span>
                                                <input type="checkbox" id="check3" class="check check2" data-wallet="cash">
                                            </label></li>
                                        <li>
                                            <p>Наличными курьеру </p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div id="box-wallet" style="display:none;">
                            <div class="row" >
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cart-left__input-name_wrapper">
                                    <div class="form-group sdacha">
                                        <label for="moneyInput" style="position: relative;
                                            bottom: 1.5rem;">Нужна сдача c:</label>
                                        <div class="money-wrapper">
                                            <input type="number" class="form-control" id="moneyInput" placeholder="">
                                            <small><?=$info[1]->text ? $info[1]->text : ''?></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="cart-left__input-name_wrapper ">
                                <div class="form-group">
                                    <label for="comInput">Комментарии к заказу:</label>
                                    <input type="text" class="form-control" id="comInput" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!-- end left col-6 -->
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="cart-right__name-title">
                            <p>Ваш заказ <span>(<?=count($_SESSION['basket']);?> товаров)</span></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="cart-right__list-product">
                            <?foreach ($_SESSION['basket'] as $value):?>
                                <ul>
                                <li>
                                    <img src="<?=$value->getImage();?>" width="100" height="75">
                                </li>
                                <li>
                                    <p class="cart-right__list-product_title"><?=$value->name?></p>
                                </li>
                                <li>
                                    <p class="cart-right__list-product_price"><span><?=$value->count?></span>x<span><?=$value->price;?></span>тг.</p>
                                </li>
                            </ul>
                            <?endforeach;?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="cart-right__footer-price">
                            <ul>
                                <li>
                                    <p>Итого:</p>
                                    <span id="sumBasket"><?=$sum?> тг.</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="cart-left__top-btn">
                            <p><?=$info[2]->text ? $info[2]->text : ''?></p>
                            <button type="button" id="button-pay" value="Оформить заказ" data-user="<?=Yii::$app->user->isGuest?>">Оформить заказ</button>
                        </div>
                    </div>
                </div>
            </div> <!-- end right col-6 -->
        </div> <!-- End row -->
    </div> <!-- End Container -->
</section>