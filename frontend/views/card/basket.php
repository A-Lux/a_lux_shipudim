<div class="content">
    <div class="main">
        <div class="inner-bg">
            <div class="parallax-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="bread-crumbs wow fadeInLeft">
                                <ul>
                                    <li><a href="<?=$menu[0]->url;?>"><?=$menu[0]->text;?></a></li>
                                    <li>/</li>
                                    <li><a href="<?=$menu[5]->url;?>"><?=$menu[5]->text;?> </a></li>
                                </ul>
                            </div>
                            <?php if(count($_SESSION['basket']) == 0):?>
                                <div class="product-card-bg wow fadeInUp empty-basket">
                                    <div class="inner-title">
                                        <h3>Ваша корзина пуста! </h3>
                                    </div>
                                </div>
                            <?else:?>
                                <div class="product-card-bg wow fadeInUp">
                                    <div class="inner-title">
                                        <h3><?=$menu[5]->text;?> </h3>
                                    </div>
                                    <div class="desktop-version table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Количество</th>
                                                <th>Стоимость</th>
                                                <th>Итог</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <? foreach ($_SESSION['basket'] as $v):?>
                                                <tr>
                                                <td>
                                                    <div class="basket-product">
                                                        <div class="image">
                                                            <img src="<?=$v->getImage();?>" alt="" height="100">
                                                        </div>
                                                        <p><?=$v->name;?></p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="count-input">
                                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation btn-operation-minus" data-id="<?=$v->id?>">-</span>
                                                        <input class="quantity input-text" min="1" name="count" value="<?=$v->count?>" data-id="<?=$v->id?>" id="countProduct<?=$v->id?>" type="number">
                                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation btn-operation-plus" data-id="<?=$v->id?>">+</span>
                                                    </div>
                                                </td>
                                                <td><?=$v->price;?> тг</td>
                                                <td><p id="sumProduct<?=$v->id?>"><?=$v->getSumProduct($v->id)?> тг</p></td>
                                                <td>
                                                    <div class="delete-icon btn-delete-product-from-basket" data-id="<?=$v->id?>">
                                                        <img src="/images/delete.png" alt="">
                                                    </div>
                                                </td>
                                            </tr>
                                            <?endforeach;?>

                                            </tbody>
                                        </table>
                                    </div>
                                   <div class="mobile-version">
                                       <? foreach ($_SESSION['basket'] as $v):?>
                                        <div class="basket-item">
                                           <div class="basket-product">
                                               <div class="image">
                                                   <img src="<?=$v->getImage();?>" alt="" height="100">
                                               </div>
                                               <p><?=$v->name;?></p>
                                           </div>
                                            <div class="count-input">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation btn-operation-minus" data-id="<?=$v->id?>">-</span>
                                                <input class="quantity input-text" min="1" name="count" value="<?=$v->count?>" data-id="<?=$v->id?>" id="countProduct<?=$v->id?>" type="number">
                                                <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation btn-operation-plus" data-id="<?=$v->id?>">+</span>
                                            </div>
                                           <div class="basket-product">
                                               <p>Стоимость <span><?=$v->price;?> тг</span></p>
                                           </div>
                                           <div class="basket-product">
                                               <p> Итог <span id="sumProduct<?=$v->id?>"><?=$v->getSumProduct($v->id)?> тг</span></p>
                                           </div>
                                           <div class="delete-icon btn-delete-product-from-basket" data-id="<?=$v->id?>">
                                              <img src="/images/delete.png" alt="">
                                           </div>
                                       </div>
                                       <?endforeach;?>
                                   </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="clear-basket btn-delete-all-from-basket">
                                                <p>Очистить корзину</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="total-cost">
                                                <p>К оплате:</p>
                                                <div class="box">
                                                    <span id="sumBasket"><?=$sum?> тг</span>
                                                    <input type="hidden" class="sumProductBasket" value="<?=$sum?>">
                                                </div>
                                            </div>
                                            <div class="total-cost">
                                                <p>Без учета стоимости доставки</p>
                                            </div>
                                            <div class="order-basket order-dark-btn">
                                                <button type="button" class="btn-basket-to-order">Расчитать стоимость</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>

                        <div class="col-sm-12">
                            <div class="inner-up-btn wow tada">
                                <a onclick="topFunction()">Наверх<img src="/images/up-inner.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
