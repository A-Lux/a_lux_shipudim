<footer class="footer">
    <div class="menu-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <a href=""><img src="<?=Yii::$app->view->params['logo']->getImage();?>" alt=""></a>
                </div>
                <div class="col-sm-2">
                    <div class="menu">
                        <!-- <img src="/images/pod.svg" alt="">
                        <p>Меню доставки</p> -->
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="menu">
                        <a href="<?=Yii::$app->view->params['menu'][6]->url;?>">
                            <p>ВАКАНСИИ</p>
                        </a>
                    </div>
                </div>

                <div class="col-sm-2 px-0">
                    <div class="phone">
                        <a href="tel:<?=Yii::$app->view->params['contact']->phone?>"><img src="/images/phone-ico.svg" alt="">
                            <?=Yii::$app->view->params['contact']->phone?></a>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="partners desktop-version">
                        <p>Наши партнеры
                            <?foreach(Yii::$app->view->params['partners'] as $value):?>
                                <a href="<?=$value->url;?>"><img src="<?=$value->getImage();?>" alt=""></a>
                            <?endforeach;?>

                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>