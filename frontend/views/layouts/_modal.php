<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="signUpModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-login modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="signUpModalLabel">Регистрация</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="email" placeholder="Email" name="SignupForm[email]">
                    </div>
                    <div class="authorization-input">
                        <input type="password" placeholder="Пароль" name="SignupForm[password]">
                    </div>
                    <div class="authorization-input">
                        <input type="password" placeholder="Повторите пароль" name="SignupForm[password_verify]">
                    </div>
                    <div class="authorization-input">
                        <input type="text" name="SignupForm[username]" placeholder="8(000) 000-0000">
                    </div>
                    <script>
                        $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                    </script>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn register-button">Регистрация</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="signInModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-login modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="signInModalLabel">Авторизация</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="email" placeholder="Email" name="LoginForm[email]">
                    </div>
                    <div class="authorization-input">
                        <input type="password" placeholder="Пароль" name="LoginForm[password]">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" data-toggle="modal" data-target="#updatePassword" id="password_">Забыли пароль?</a>
                    <button type="button" class="btn login-button">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="updatePassword" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="updatePassword">Забыли пароль?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="text" placeholder="Ваш email" name="ForgotYourPassword[email]">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn sendForgotPassword">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>
