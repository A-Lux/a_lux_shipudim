
    <div class="menu-bg">
        <div class="container">
            <div class="row hide-load">
                <div class="col-sm-1">
                    <div class="logo">
                        <a href="/"><img src="<?=Yii::$app->view->params['logo']->getImage();?>" alt=""></a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="menu-row">
                        <div class="menu">
                            <div class="new-menu">
                                <div>
                                    <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Меню доставки
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/catalog/sales">
                                            <?=Yii::$app->view->params['header'][7]->text;?>
                                        </a>
                                        <?foreach (Yii::$app->view->params['catalog'] as $value):?>
                                            <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['catalog/index', 'url' => $value->url])?>">
                                                <?=$value->name;?>
                                            </a>
                                        <?endforeach;?>

                                    </div>
                                </div>
                                <div><a href="<?=Yii::$app->view->params['menu'][10]->url;?>">
                                        <?=Yii::$app->view->params['menu'][10]->text;?>
                                    </a> </div>
                                <div>
                                    <a class=" dropdown-toggle" href="<?=Yii::$app->view->params['menu'][1]->url;?>">
                                        <?=Yii::$app->view->params['menu'][1]->text;?>
                                    </a>

                                    <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">1</a>
                                        <a class="dropdown-item" href="#">2n</a>
                                        <a class="dropdown-item" href="#">3n</a>
                                    </div> -->
                                </div>
                            </div>

                            <!-- <img src="images/pod.svg" alt="">
                        <p>Меню доставки</p>
                        <div class="toggle-menu">
                            <ul>
                                <li><a href="catalog.php">Каталог</a></li>
                                <li><a href="product-card.php">Карточка товара</a></li>
                                <li><a href="basket.php">Корзина </a></li>
                                <li><a href="vacancy.php">Вакансии</a></li>
                                <li><a href="cabinet.php">Личный кабинет</a></li>
                            </ul>
                        </div>
                        <div class="menu-cover" id="close-menu">
                            <div class="hamburger"></div>
                        </div> -->
                        </div>
                        <content class="mob-view">
                            <input id="hamburger" class="hamburger" type="checkbox">
                            <label class="hamburger" for="hamburger">
                                <i></i>
                                <text style="display:none;">
                                    <close>закрыть</close>
                                    <open>меню</open>
                                </text>
                            </label>
                            <section class="drawer-list">

                                <ul>

                                    <li>
                                        <div class="phone mobile-version">
                                            <a href="tel:<?=Yii::$app->view->params['contact']->phone?>">
                                                <img src="/images/phone-ico.svg" alt="">
                                                <?=Yii::$app->view->params['contact']->phone?></a>
                                        </div>
                                    </li>
<!--                                    <li>-->
<!--                                        <a data-id="1" href="">Каталог</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a data-id="2" href="">Карточка товара</a>-->
<!--                                    </li>-->
                                    <li>
                                        <a data-id="3" href="<?=Yii::$app->view->params['menuMobile'][5]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][5]->text;?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="4" href="<?=Yii::$app->view->params['menuMobile'][6]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][6]->text;?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="5" href="<?=Yii::$app->view->params['menuMobile'][9]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][9]->text;?>
                                        </a>
                                    </li>
                                    <li class="dropdown-btn">
                                        <a data-id="6">Меню доставки</a>
                                    </li>
                                    <div class="dropdown-menu-mobile">
                                        <ul>
                                           <a class="dropdown-item" href="/catalog/sales">
                                            <?=Yii::$app->view->params['header'][7]->text;?>
                                        </a>
                                        <?foreach (Yii::$app->view->params['catalog'] as $value):?>
                                            <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['catalog/index', 'url' => $value->url])?>">
                                                <?=$value->name;?>
                                            </a>
                                        <?endforeach;?>
                                        </ul>
                                    </div>
                                    <li>
                                        <a data-id="7" href="<?=Yii::$app->view->params['menuMobile'][10]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][10]->text;?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="8" href="<?=Yii::$app->view->params['menuMobile'][1]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][1]->text;?>
                                        </a>
                                    </li>
                                </ul>
                            </section>
                        </content>
                        <form action="/search/index">
                            <div class="search" id="search">
                                <input type="text" name="text" autocomplete="off" onfocus="this.value=''">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-2 px-0">
                    <? if(Yii::$app->user->isGuest):?>
                        <a href="#" class="registr" data-toggle="modal" data-target="#signInModal">Войти</a>
                        <span class="registr" >/</span>
                        <a href="#" class="registr" data-toggle="modal" data-target="#signUpModal">Регистрация</a>
                    <?  else:?>
                        <a href="/account" class="registr"><?=Yii::$app->user->identity->email;?></a>
                            <span class="registr" >/</span>
                        <a href="/account/logout" class="registr">Выйти</a>
                    <?endif;?>
                    <div class="phone desktop-version">
                        <a href="tel:<?=Yii::$app->view->params['contact']->phone?>"><img src="/images/phone-ico.svg" alt="">
                            <?=Yii::$app->view->params['contact']->phone?></a>
                    </div>
                </div>
                <div class="col-sm">
                    <!-- <div class="lang">
                    <img src="images/world-ico.svg" alt="">
                    <select name="" id="">
                        <option value="">RU</option>
                        <option value="">KZ</option>
                    </select>
                </div> -->

                </div>
                <!-- <div class="basket-wrap">
                <div class="mobile-version basket">
                    <a href="">
                        <img src="images/basket-ico.svg" alt="">
                        <div class="circle">1</div>
                    </a>
                </div>
            </div> -->
                <div class="basket-wrap">
                    <div class="desktop-version basket">
                        <a href="/card">
                            <img src="/images/basket-ico.svg" alt="">
                            <div class="circle">
                                <span id="countBasket" class="count"><?= count($_SESSION['basket']);?></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="socials">
        <ul>
            <li><a href="<?=Yii::$app->view->params['contact']->phone?>"><i class="fas fa-phone"></i></a></li>
            <li><a href="<?=Yii::$app->view->params['contact']->facebook?>"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="<?=Yii::$app->view->params['contact']->instagram?>"><i class="fab fa-instagram"></i></a></li>
        </ul>
    </div>
