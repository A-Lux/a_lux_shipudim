<header class="header">
    <div class="video-overlay">
    </div>

        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="<?=Yii::$app->view->params['banner']->getVideo();?>" type="video/mp4">
        </video>

    <div class="menu-bg">
        <div class="container">
            <div class="row hide-load">
                <div class="col-xl-1 col-md-12">
                    <div class="logo">
                        <a href="/"><img src="<?=Yii::$app->view->params['logo']->getImage();?>" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-8 col-md-12">
                    <div class="menu-row">
                        <div class="menu">
                            <div class="new-menu">
                                <div>
                                    <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Меню доставки
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/catalog/sales">
                                            <?=Yii::$app->view->params['header'][7]->text;?>
                                        </a>
                                        <?foreach (Yii::$app->view->params['catalog'] as $value):?>
                                            <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['catalog/index', 'url' => $value->url])?>">
                                                <?=$value->name;?>
                                            </a>
                                        <?endforeach;?>

                                    </div>
                                </div>
                                <div><a href="<?=Yii::$app->view->params['menu'][10]->url;?>">
                                        <?=Yii::$app->view->params['menu'][10]->text;?>
                                    </a> </div>
                                <div>
                                    <a class=" dropdown-toggle" href="<?=Yii::$app->view->params['menu'][1]->url;?>">
                                        <?=Yii::$app->view->params['menu'][1]->text;?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <content class="mob-view">
                            <input id="hamburger" class="hamburger" type="checkbox">
                            <label class="hamburger" for="hamburger">
                                <i></i>
                                <text style="display:none;">
                                    <close>закрыть</close>
                                    <open>меню</open>
                                </text>
                            </label>
                            <section class="drawer-list">
                                <ul>

                                    <li>
                                        <div class="phone mobile-version">
                                            <a href="tel:<?=Yii::$app->view->params['contact']->phone?>">
                                                <img src="/images/phone-ico.svg" alt="">
                                                <?=Yii::$app->view->params['contact']->phone?></a>
                                        </div>
                                    </li>
<!--                                    <li>-->
<!--                                        <a data-id="1" href="">Каталог</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a data-id="2" href="">Карточка товара</a>-->
<!--                                    </li>-->
                                    <li>
                                        <a data-id="3" href="<?=Yii::$app->view->params['menuMobile'][5]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][5]->text;?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="4" href="<?=Yii::$app->view->params['menuMobile'][6]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][6]->text;?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="5" href="<?=Yii::$app->view->params['menuMobile'][9]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][9]->text;?>
                                        </a>
                                    </li>
                                    <li class="dropdown-btn">
                                        <a data-id="6">Меню доставки <i class="fas fa-sort-down"></i></a>
                                    </li>
                                    <div class="dropdown-menu-mobile">
                                        <ul>
                                           <a class="dropdown-item" href="/catalog/sales">
                                            <?=Yii::$app->view->params['header'][7]->text;?>
                                        </a>
                                        <?foreach (Yii::$app->view->params['catalog'] as $value):?>
                                            <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['catalog/index', 'url' => $value->url])?>">
                                                <?=$value->name;?>
                                            </a>
                                        <?endforeach;?>
                                        </ul>
                                    </div>
                                    <li>
                                        <a data-id="7" href="<?=Yii::$app->view->params['menuMobile'][10]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][10]->text;?>
                                        </a>
                                    </li>
                                      <li>
                                        <a data-id="8" href="<?=Yii::$app->view->params['menuMobile'][1]->url;?> ">
                                            <?=Yii::$app->view->params['menuMobile'][1]->text;?>
                                        </a>
                                    </li>                                
                                </ul>
                            </section>
                        </content>
                        <form action="/search/index">
                            <div class="search" id="search">
                                <input type="text" name="text" autocomplete="off" onfocus="this.value=''">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-xl-2 px-0 col-md-6">
                    <? if(Yii::$app->user->isGuest):?>
                        <a href="#" class="registr" data-toggle="modal" data-target="#signInModal">Войти</a>
                        <span class="registr" >/</span>
                        <a href="#" class="registr" data-toggle="modal" data-target="#signUpModal">Регистрация</a>
                    <?  else:?>
                        <a href="/account" class="registr"><?=Yii::$app->user->identity->email;?></a>
                        <span class="registr" >/</span>
                        <a href="/account/logout" class="registr">Выйти</a>
                    <?endif;?>

<!--                    <a href="#" class="registr" id="signIn">Войти</a>-->
<!--                    <span class="registr" >/</span>-->
<!--                    <a href="#" class="registr" id="signUp">Регистрация</a>-->
                    <div class="phone desktop-version">
                        <a href="tel:<?=Yii::$app->view->params['contact']->phone?>"><img src="/images/phone-ico.svg" alt="">
                            <?=Yii::$app->view->params['contact']->phone?></a>
                        <br>
                        <a href="tel:<?=Yii::$app->view->params['contact']->whatsap?>"><img src="/images/phone-ico.svg" alt="">
                            <?=Yii::$app->view->params['contact']->phone?></a>
                    </div>
                </div>
                <div class="col-sm">
                    <!-- <div class="lang">
                    <img src="images/world-ico.svg" alt="">
                    <select name="" id="">
                        <option value="">RU</option>
                        <option value="">KZ</option>
                    </select>
                </div> -->

                </div>
                <!-- <div class="basket-wrap">
                <div class="mobile-version basket">
                    <a href="">
                        <img src="images/basket-ico.svg" alt="">
                        <div class="circle">1</div>
                    </a>
                </div>
            </div> -->
                <div class="basket-wrap">
                    <div class="desktop-version basket">
                        <a href="/card">
                            <img src="/images/basket-ico.svg" alt="">
                            <div class="circle">
                                <span id="countBasket" class="count"><?= count($_SESSION['basket']);?></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!-- <div class="basket-wrap">
                <div class="desktop-version basket">
                    <a href="">
                        <img src="images/basket-ico.svg" alt="">
                        <div class="circle">1</div>
                    </a>
                </div>
            </div> -->
            </div>
            <div class="col-sm-12">
                <div class="main-text">
                    <h4><?=Yii::$app->view->params['mainSub'][0]->name?>
                        <!-- <div class="new">
                        <p>NEW!</p>
                    </div> -->
                    </h4>
                    <!-- <p>Самое богатое и вкусное ассорти шашлыка для вашей большой компании!</p>
                <h5>3 литра пива или
                    компота в подарок!</h5>
                <div class="price">
                    <p>20 000 тг.</p>
                </div> -->
                </div>
                <div class="order-btn">
                    <a href="#section3">Заказать</a>
                </div>
            </div>
            <div class="col-sm-12">
                <!-- <div class="partners desktop-version">
                    <p>Наши партнеры
                        <?foreach(Yii::$app->view->params['partners'] as $value):?>
                            <a href="<?=$value->url;?>"><img src="<?=$value->getImage();?>" alt=""></a>
                        <?endforeach;?>
                    </p>
                </div>
                <div class="partners mobile-version">
                    <p>Наши партнеры </p>
                    <?foreach(Yii::$app->view->params['partners'] as $value):?>
                        <a href="<?=$value->url;?>"><img src="<?=$value->getImage();?>" alt=""></a>
                    <?endforeach;?>
                </div> -->
            </div>
        </div>
    </div>
    <div class="socials">
        <ul>
            <li><a href="<?=Yii::$app->view->params['contact']->phone?>"><i class="fas fa-phone"></i></a></li>
            <li><a href="<?=Yii::$app->view->params['contact']->facebook?>"  target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="http://<?=Yii::$app->view->params['contact']->instagram?>"  target="_blank"><i class="fab fa-instagram"></i></a></li>
        </ul>
    </div>
</header>
