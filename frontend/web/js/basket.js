$('input[name="name_and_surname"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});


$("body").on('click','.btn-to-basket',function () {

    var id = $(this).attr('data-id');
    var amount = $('.count'+id).val();
    $.ajax({
        url: "/card/add-product",
        type: "GET",
        dataType: "json",
        data: {id:id, amount:amount},

        success: function (data) {
            if(data.status) {
                $('#countBasket').html(data.count);
                $('body').overhang({
                    type: "info",
                    html: true,
                    message: "Продукт успешно добавлен в корзину!",
                    duration: 3,
                    closeConfirm: true
                });
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }

    });
});

$('body').on('click', '.btn-basket-to-order', function () {
    sum = $('.sumProductBasket').val();

    if(sum < 3000){
        swal("Минимальная сумма заказа составляет 3000 тг");
    }else{
        window.location.href = "/card/order";
    }
    //alert(sum);
});


function updatePrice()
{
    $.get("/card/sum-basket", function( data ) {
        $("#sumBasket").html(data+" тг.");
    });
}


$("body").on('click','.btn-delete-product-from-basket',function (){
    var id = $(this).attr('data-id');
    var container = $(this).parent().parent();

    $.get("/card/delete-product-ajax?id="+id, function( data ) {
        if(data == 1) {
            container.remove();
            updatePrice();

        }
    });
});

$("body").on('click','.btn-delete-all-from-basket',function (){
    swal({
        title: "Вы уверены?",
        text: "что хотите очистить корзину!",
        icon: "warning",
        buttons: ["Отмена", "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-all";
            } else {

            }
        });
});

$("body").on('click','.btn-operation-minus',function (){
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/down-clicked",
        type: "GET",
        dataType: "json",
        data: {id:id},
        success: function(data){
            $('#countProduct'+id).html(data.countProduct);
            $('#sumBasket').html(data.sum+" <span>тг.</span>");
            $('.sumProductBasket').val(data.sum);
            $('#sumProduct'+id).html(data.sumProduct+" <span>тг.</span>");
        },
        error: function () {
            // alert('FAILED');

        }
    });
});

$("body").on('click','.btn-operation-plus',function (){
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/up-clicked",
        type: "GET",
        dataType: "json",
        data: {id:id},
        success: function(data){
            $('#countProduct'+id).html(data.countProduct);
            $('.sumProductBasket').val(data.sum);
            $('#sumBasket').html(data.sum+"<span>тг.</span>");
            $('#sumProduct'+id).html(data.sumProduct+"<span>тг.</span>");

        },
        error: function () {

        }
    });
});

$("body").on("change paste keyup",'.quantity', function () {
    var id = $(this).attr('data-id');
    var v = $(this).val();
    $.ajax({
        url: "/card/count-changed",
        type: "GET",
        dataType: "json",
        data: {id:id,v:v,deliveryPrice:deliveryPrice},
        success: function(data){
            $('#countProduct'+id).html(data.countProduct);
            $('#sumBasket').html(data.sum+"<span>тг.</span>");
            $('#sumBasket').attr("data-price",data.sumWithoutDelivery);
            $('.sumProductBasket').val(data.sum);
            $('#sumProduct'+id).html(data.sumProduct+" <span>тг.</span>");
        },
        error: function () {
            // alert('FAILED');
        }
    });
});

$("#button-pay").click(function () {

    if($('#checkSpanCourier').attr('data-tab') == '1') {
        // По Курьеру
        var typePay = 0;
        // Банковская карта
        if ($("#checkSpanCard").attr('data-tab') == '1') {
            typePay = 1;
        }else if($("#checkSpanCash").attr('data-tab') == '1'){
            typePay = 2;
        }

        if(typePay !== 0) {
            var order_id = $('.order_id').val();
            var address = $("#deliveryCity").val() + (', ') + $("#deliveryAddress").val();
            var username = $("#nameInput").val();
            var phone = $("#telInput").val();
            var comment = $("#comInput").val();
            var change = $("#moneyInput").val();

            phoneStatus = true;
            for (var j = 0; j < phone.length; j++) {
                if (phone.charAt(j) == '_') {
                    phoneStatus = false;
                }
            }

            if(address == ""){
                swal('Пожалуйста!', "Укажите Ваш адрес!", 'error');
            }else if(username == ""){
                swal('Пожалуйста!', "Заполните контактные данные!", 'error');
            }else if(!phoneStatus || phone == 0) {
                swal('Телефон должен состоять из 10 цифров');
            }else if(typePay == 2){
                if(change == 0 || change == ""){
                    swal('Пожалуйста!', "Введите сумму с которой понадобится сдача при оплате заказа.!", 'error');
                }else{
                    $.ajax({
                        type: "GET",
                        url: "/card/order-by-courier",
                        data: {
                            change: change, address: address, typePay: typePay, username: username,
                            phone: phone, comment: comment, order_id: order_id,
                        },
                        success: function (data) {
                            $('#button-pay').html('Оплатить');
                            $('#button-pay').html('<i class="fa fa-spinner fa-spin"></i> Оформить заказ');
                            $('#button-pay').removeAttr("disabled");
                            if (data == 1) {
                                swal('Спасибо за сделанный заказ! Номер заказа № '+order_id,
                                    'В ближайшее время наш менеджер свяжется с Вами!', 'success');
                                setTimeout(function () {
                                    window.location.href = "/";
                                }, 8000);
                            } else if (data == 0) {
                                swal('Ошибка!', "Что-то не так!", 'error');
                            } else {
                                swal('Ошибка!', data, 'error');
                            }

                        },
                        error: function () {
                            $('#button-pay').html('Оплатить');
                            $('#button-pay').removeAttr("disabled");
                            swal('Ошибка!', "Что-то пошло не так!", 'error');
                        }
                    });
                }
            }else {
                $.ajax({
                    type: "GET",
                    url: "/card/order-by-courier",
                    data: {
                        change: change, address: address, typePay: typePay, username: username,
                        phone: phone, comment: comment, order_id: order_id,
                    },
                    success: function (data) {
                        $('#button-pay').html('Оплатить');
                        $('#button-pay').html('<i class="fa fa-spinner fa-spin"></i> Оформить заказ');
                        $('#button-pay').removeAttr("disabled");
                        if (data == 1) {
                            swal('Спасибо за сделанный заказ! Номер заказа № '+order_id,
                                'В ближайшее время наш менеджер свяжется с Вами!', 'success');
                            setTimeout(function () {
                                window.location.href = "/";
                            }, 8000);
                        } else if (data == 0) {
                            swal('Ошибка!', "Что-то не так!", 'error');
                        } else {
                            swal('Ошибка!', data, 'error');
                        }

                    },
                    error: function () {
                        $('#button-pay').html('Оплатить');
                        $('#button-pay').removeAttr("disabled");
                        swal('Ошибка!', "Что-то пошло не так!", 'error');
                    }
                });
            }
        }else{
            swal('Пожалуйста!', "Выберите тип оплаты!", 'error');
        }

    }else{
        $('#button-pay').html('Оформить заказ');
        $('#button-pay').removeAttr("disabled");
        swal('Ошибка!',"Чтобы оформить заказ вам нужно выбрать тип доставки!", 'error');
    }

});

