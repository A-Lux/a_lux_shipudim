(function($) {
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fas fa-chevron-up"></i></div><div class="quantity-button quantity-down"><i class="fas fa-chevron-down"></i></div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
    $(window).load(function() {
        $('.before-load img').addClass('top-right');
        $('.before-load').find('img').fadeOut().end().delay(400).fadeOut('slow');
        $('.hide-load').hide(0).delay(400).show();
    });
    $('.img-item').click(function (e) {
        e.preventDefault();
        $('.img-item').hide();
        $('#expandedImg').addClass('wow animated fadeIn').show();
    });
    $('#expandedImg').click(function () {
        $('.img-item').show();
        $('#expandedImg').removeClass('wow animated fadeIn').hide();
    });
    $('.favorite-btn').hover(function (e) {
        e.preventDefault();
        $('.favorite-btn img').toggleClass('wow animated heartBeat');
    })
    $('.like img').hover(function (e) {
        e.preventDefault();
        $(this).toggleClass('wow animated heartBeat');
    });
    $('.show-more').click(function () {
        $('.show-more img').toggleClass('rotate');
    });
    $('.inner-menu-list img').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('rotate');
    });
    const el = $('.menu-cover');
    el.on('click', function() {
        el.toggleClass('is-active');
    });

    $('.menu-cover').click(function () {
        $('.toggle-menu').toggle(500);

    })
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:true
            },
            800:{
                items:3,
                nav:true
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    })

    // $('.owl-carousel-newSlider').owlCarousel({
    //     loop:true,
    //     margin:10,
    //     responsiveClass:true,
    //     responsive:{
    //         0:{
    //             items:1,
    //             nav:true
    //         },
    //         600:{
    //             items:1,
    //             nav:true
    //         },
    //         800:{
    //             items:1,
    //             nav:true
    //         },
    //         1000:{
    //             items:1,
    //             nav:true,
    //             loop:true
    //         }
    //     }
    // })

    new WOW().init();
})(jQuery);


$(document).ready(function () {
    $('.dropdown-btn').click(function(){
        $('.dropdown-menu-mobile').slideToggle('dropdown-menu-mobile-active');
    });
    $('#password_').click(function(){
        $('.show').hide();
    })
    $('.plus').click(function () {
        let valInput = $('.quantity_box-input').attr('value');
        var integer = parseInt(valInput, 10);
        let num = integer + 1;
        $('.quantity_box-input').attr('value', num);
    });
    $('.minus').click(function () {
        let valInput = $('.quantity_box-input').attr('value');
        var integer = parseInt(valInput, 10);
        let num = integer - 1;
        if (num > 0) {
            $('.quantity_box-input').attr('value', num);
        }
    });
    $('.more-btn__text').click(function (e) {
        e.preventDefault();
        let btnMore = $(this).attr('data-btn');
        let attrBtn = $(this).attr('href');
        if (btnMore == "1") {
            $(this).html('Скрыть');
            $(this).attr('data-btn', '0');
            $('#dots').css('display', 'none');
            $(attrBtn).removeClass('inactive-text__wrapper');

        } else {
            $(this).html('Подробнее <img src="images/top.png">');
            $(this).attr('data-btn', '1');
            $('#dots').css('display', 'inline-block');
            $(attrBtn).addClass('inactive-text__wrapper');
        }
    });

});

const spanCheck = document.getElementsByClassName('checkSpan');
const spanCheck2 = document.getElementsByClassName('checkSpan2');
for (const i of document.getElementsByClassName('check1')) {
    i.addEventListener('change', e => {
        //console.log(e.target);
        for (const j of spanCheck) {
            j.classList.remove('active');
        }
        let role = i.getAttribute('data-role');
        if(role == "courier"){
            $('#box-courier').animate({height: 'show'}, 500);
        }else if(role == "pickup"){
            $('#box-courier').animate({height: 'hide'}, 500);
        }
        //console.log(role);
        // if (e.target.checked) {
        //     e.target.previousElementSibling.classList.add('active')
        // } else {
        //     e.target.previousElementSibling.classList.remove('active')
        // }
        e.target.previousElementSibling.classList.add('active');
        e.target.previousElementSibling.setAttribute("data-tab","1");
    })
}
for (const i of document.getElementsByClassName('check2')) {
    i.addEventListener('change', e => {
        //console.log(e.target);
        for (const j of spanCheck2) {
            j.classList.remove('active');
            j.setAttribute('data-tab','0');
        }
        let role = i.getAttribute('data-wallet');
        if(role == "cash"){
            $('#box-wallet').animate({height: 'show'}, 500);
        }else if(role == "card"){
            $('#box-wallet').animate({height: 'hide'}, 500);
        }
        // if (e.target.checked) {
        //     e.target.previousElementSibling.classList.add('active')
        // } else {
        //     e.target.previousElementSibling.classList.remove('active')
        // }
        e.target.previousElementSibling.classList.add('active');
        e.target.previousElementSibling.setAttribute("data-tab","1");
    })
}




$(document).mouseup(function(e)
{
    var container = $("#expandedImg");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.hide();
        $('.img-item').show();
        $('#expandedImg').removeClass('wow animated fadeIn').hide();
    }
});
function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg.src = imgs.src;
    imgText.innerHTML = imgs.alt;
    expandImg.parentElement.style.display = "block";
}

function topFunction() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}


document.getElementById("search").addEventListener("click",  () => {
    var menu = document.getElementById("close-menu");
    if (menu.classList.contains("is-active") ) {
        menu.click()
    }
});

// <<<<<<< HEAD
// =======

//
// const vacancy = document.querySelectorAll(".vacancySubmit");
// for (const i of vacancy) {
//     i.addEventListener('click', function(){
//         Swal.fire({
//             title: 'Вакансия',
//            html: '<input placeholder="ФИО"></input>'+ '<br><br>' +'<input placeholder="Телефон"></input>'
//                  + '<br><br>' +'<textarea placeholder="Раскажите о себе"></textarea>'
//           })
//     })
// }


// >>>>>>> d2242a216f9aab2de79438afcc8e6c5d6759f976

new Glide('.glide').mount();

