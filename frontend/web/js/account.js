

$('input[name="UserProfile[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="UserProfile[surname]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});


$('input[name="UserProfile[father]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});



$("body").on('click', '.register-button', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/account/register',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                window.location.href = "/account";
            }else{
                swal('Ошибка!', response, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '.login-button', function (e) {
    $.ajax({
        type: 'POST',
        url: '/account/login',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                window.location.href = "/account";
            }else{
                swal('Ошибка!', response, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.update-profile-button', function (e) {
    $.ajax({
        type: 'POST',
        url: '/account/update-account-data',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){

                swal('Ваши изменения',' успешно сохранен.', 'success');
                setTimeout(function () {
                    window.location.href = "/account";
                }, 3000);
            }else{
                swal('Ошибка!', response, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.update-password-button', function (e) {
    $.ajax({
        type: 'GET',
        url: '/account/update-password',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if(data == 1){
                swal('Ваши изменения',' успешно сохранен.', 'success');
                setTimeout(function () {
                    window.location.href = "/account";
                }, 2000);
            }else{
                swal('Ошибка!', data, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', ".sendForgotPassword", function () {
    $.ajax({
        type: "POST",
        url: "/account/forgot-password",
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if (data == 1) {
                swal('Ваш старый пароль сброшен!','Новый пароль отправлен на Вашу электронную почту', 'success');
                setTimeout(function () {

                    window.location.href = "/";
                }, 2500);
            } else {
                swal(data);
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});



$(".btn-add-favorite").click(function () {
    var product_id = $(this).attr('data-id');
    var user_id = $(this).attr('data-user-id');

    if(user_id == ""){
        swal('Ошибка!', 'Чтобы добавить товар в избранное нужно войти!');
    }else{
        $.ajax({
            url: "/account/add-to-favorite",
            dataType: "json",
            data: {product_id:product_id,user_id:user_id},
            type: "GET",
            success: function(data){
                swal(data.text);
            },
            error: function () {
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});



$(".btn-delete-product-from-favorite").click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/account/delete-from-favorite",
        data: {id:id},
        type: "GET",
        success: function(data){
            if(data == 0){
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }else{
                $('#nav-profile').html(data);
                setTimeout(function () {

                    window.location.href = "/account";
                }, 3000);
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});
