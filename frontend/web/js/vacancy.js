$('input[name="Respond[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="Respond[phone]"]').inputmask("8(999) 999-9999");


$('body').on('click', '.respond-vacancy-btn', function (e) {

     var vacancy_id = $(this).attr('data-vacancy-id');
     var formData = $(this).closest('form').serialize();

     console.log(vacancy_id);

    $.ajax({
        type: 'POST',
        url: '/content/respond',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if(data == 1) {
                swal('Ваши данные', ' успешно отправлены.', 'success');
                setTimeout(function () {
                    window.location.href = "/content/vacancy";
                }, 2000);
            }else if(data == 2){
                swal('Ваши данные', 'успешно отправлены!', 'success');
                setTimeout(function () {
                    window.location.href = "/content/vacancy";
                }, 2000);
            }else{
                swal('Ошибка!', data, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '.attach-file.shipudim-form', function (e) {
    e.preventDefault();
    console.log(e.target.attributes[0]);
    $('#'+e.target.attributes[0].value).click();
});

var file;
var data_file = new FormData();

$('body').on('change', '.uploadImgInputForm',function(e){
    file = this.files;

    $.each( file, function( key, value ){
        data_file.append( 'Files['+key+']', value );
    });

    e.stopPropagation();    // Остановка происходящего
    e.preventDefault();     // Полная остановка происходящего

    $.ajax({
        url: '/content/form-files',
        data: data_file,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $(response).appendTo('.modal-files.file-form');
            data_file = new FormData();
        },
        error: function () {
            swal(response, 'error');
        }
    });
});

