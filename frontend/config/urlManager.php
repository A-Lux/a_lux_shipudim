<?php

/** @var array $params */

return [
        'class' => 'yii\web\UrlManager',
    //    'hostInfo' => 'http://azialife',
        'baseUrl' => '',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'cache' => false,
        'rules' => [
            'catalog/sales/<page:\d+>' => 'catalog/sales',
            'catalog/sales' => 'catalog/sales',

            'catalog/<url:[\w_\/-]+>/<page:\d+>' => 'catalog/index',
            'catalog/products/<url:[\w_\/-]+>' => 'catalog/product-card',
            'catalog/<url:[\w_\/-]+>' => 'catalog/index',

            'account/<page:\d+>' => 'account/index',

            'search/index' => 'search/index',
            'catalog/<id:\d+>' => 'catalog/category',

    ],
];