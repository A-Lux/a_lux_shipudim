<?php
return [
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'sdulife.kz@gmail.com',
                'password' => 'lmjbgcdza',
                'port' => '465', // Port 25 is a very common port too
                'encryption' => 'ssl', // It is often used, check your provider or mail server specs
            ],
            'useFileTransport' => false,
        ],
        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
//            'language' => 'ru-RU',
            'class' => 'yii\i18n\Formatter',
        ],

        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
    ],
];
