<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "respond_files".
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string $name
 * @property string $phone
 * @property string $text
 * @property int $created_at
 *
 */


class Respond extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'respond';
    }

    public function rules()
    {
        return [
            [['vacancy_id', 'name', 'phone', 'text'], 'required'],
            [['vacancy_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Вакансия',
            'name' => 'ФИО',
            'phone' => 'Номер телефона',
            'text' => 'Текст',
            'created_at' => 'Дата отклика',
        ];
    }

    public function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getVacancy()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'vacancy_id']);
    }

    public function getVacancyName(){
        return (isset($this->vacancy))? $this->vacancy->title:'Не задан';
    }

    public function getFile()
    {
        return $this->hasOne(RespondFiles::className(), ['parent_id' => 'id']);
    }

    public function getFilePath()
    {
        return ($this->file->name) ? '/uploads/resume/' . $this->file->name : '';
    }

}
