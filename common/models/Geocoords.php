<?php
namespace common\models;

use Yii;

class Geocoords extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'geocoords';
    }

    public function rules()
    {
        return [
            [['coords', 'summ'], 'required'],
            [['coords', 'name'], 'string'],
            [['summ'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coords' => 'Координаты',
            'summ' => 'Сумма доставки',
            'name' => 'Время доставки',
        ];
    }

}
