<?php
namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property string $username
 * @property string $phone
 * @property int $statusPay
 * @property int $statusProgress
 * @property int $sum
 * @property int $change
 * @property string $address
 * @property string $street
 * @property string $commentOrder
 * @property int $typePay
 * @property int $typeDelivery
 * @property int $created_at
 */

class Orders extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [['product_id', 'statusPay', 'statusProgress', 'sum', 'change', 'typePay', 'typeDelivery'], 'integer'],
            [['username', 'phone', 'sum', 'address', 'typePay'], 'required'],
            [['created_at'], 'safe'],
            [['username', 'phone', 'address', 'street', 'commentOrder'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'username' => 'ФИО клиента',
            'phone' => 'Телефон',
            'product_id' => 'Номер заказа',
            'statusPay' => 'Статус оплаты',
            'statusProgress' => 'Статус доставки',
            'sum' => 'Сумма заказа',
            'change' => 'Сдача',
            'address' => 'Адрес',
            'street' => 'Пересечении улиц',
            'commentOrder' => 'Комментарий к заказу',
            'typePay' => 'Тип оплаты',
            'typeDelivery' => 'Тип доставки',
            'created_at' => 'Дата заказа',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    public function getProducts()
    {
        return $this->hasMany(OrderedProduct::className(), ['order_id' => 'product_id']);
    }

    public static function getAll($pageSize=6)
    {
        $query =  Orders::find()->where(['user_id'=>Yii::$app->user->id])->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize,'pageSizeParam' => false, 'forcePageParam' => false]);
        $orders = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $orders;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getPayStatus()
    {
        return $this->hasOne(StatusPay::className(), ['id' => 'statusPay']);
    }

    public function getPayStatusName()
    {
        return (isset($this->payStatus)) ? $this->payStatus->name : 'не задан';
    }

    public function getProgressStatus()
    {
        return $this->hasOne(StatusProgress::className(), ['id' => 'statusProgress']);
    }

    public function getProgressStatusName()
    {
        return (isset($this->progressStatus))?$this->progressStatus->name: 'не задан';
    }
}
