<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int order_id
 * @property int $product_id
 * @property int $count
 */

class OrderedProduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderedProduct';
    }

    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count'], 'required'],
            [['order_id', 'product_id', 'count'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }
}
