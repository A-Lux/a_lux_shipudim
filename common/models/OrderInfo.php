<?php
namespace common\models;

use Yii;

class OrderInfo extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderInfo';
    }

    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
        ];
    }
}
