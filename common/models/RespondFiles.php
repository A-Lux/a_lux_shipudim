<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "respond_files".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 */

class RespondFiles extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'respond_files';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Название',
        ];
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
