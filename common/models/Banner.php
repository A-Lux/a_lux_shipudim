<?php
namespace common\models;

use Yii;
/**
 * This is the model class for table "Banner".
 *
 * @property int $id
 * @property string $image
 * @property string $video
 * @property string $name
 * @property string $status
 */

class Banner extends \yii\db\ActiveRecord
{

    public $path = 'images/banner/';

    public static function tableName()
    {
        return 'banner';
    }

    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['name'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
            [['video'], 'file', 'extensions' => 'mp4'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Текст',
            'status' => 'Статус',
            'image' => 'Изображение',
            'video' => 'Видео',
        ];
    }

    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public function getVideo()
    {
        return '/backend/web/'.$this->path.''.$this->video;
    }
}
