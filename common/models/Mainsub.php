<?php
namespace common\models;

use Yii;

class Mainsub extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'mainsub';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
