<?php
namespace common\models;

use Yii;


/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $father
 * @property string $address
 */

class UserProfile extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_profile';
    }

    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'surname', 'father', 'address'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'father' => 'Отчество',
            'address' => 'Адрес',
        ];
    }

    public function fields()
    {
        return [
            'name'
        ];
    }

    public function getFio(){
        return $this->name.' '.$this->surname;
    }
}
