<?php
namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $articul
 * @property string $image
 * @property int $price
 * @property string $content
 * @property int $status
 * @property int $url
 * @property int $isNew
 * @property int $isDiscount
 * @property int $isHit
 * @property int $top_month
 * @property int $newPrice
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $created_at
 * @property int $count
 */

class Products extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/products/';
    public $count;

    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['name', 'articul', 'url', 'status', 'price', 'category_id'], 'required'],
            [['status', 'isNew', 'isDiscount', 'isHit', 'top_month', 'category_id'], 'integer'],
            [['created_at'], 'safe'],
            [['content', 'metaDesc', 'metaKey'], 'string'],
            [['name', 'articul', 'url', 'metaName'], 'string', 'max' => 255],
            [['price', 'newPrice'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'articul' => 'Модель',
            'image' => 'Изображение',
            'price' => 'Цена',
            'content' => 'Описание',
            'status' => 'Статус',
            'url' => 'URL',
            'isNew' => 'Новая',
            'isDiscount' => 'Скидка',
            'isHit' => 'Популярная',
            'top_month' => 'Топ месяца',
            'newPrice' => 'Новая цена',
            'catalog_id' => 'Каталог',
            'category_id' => 'Категория',
            'subcategory_id' => 'Подкатегория',
            'metaName' => 'Мета названия',
            'metaDesc' => 'Мета описание',
            'metaKey' => 'Ключевые слова',
            'created_at' => 'Дата создания',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Products::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path.''.$this->image : '/no-image.png';
    }

    public function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getCategory()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'category_id']);
    }

    public function getCategoryName(){
        return (isset($this->category))? $this->category->name:'Не задан';
    }

    public static function getSum()
    {
        $sum = 0;

        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($v->isDiscount){
                    $price = $v->newPrice;
                }else{
                    $price = $v->price;
                }
                $sum+=(int)$v->count*(int)$price;
            }
        }
        return $sum;
    }


    public static function getSumProduct($id)
    {
        $sum = 0;

        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($v->id == $id){
                    if($v->isDiscount){
                        $price = $v->newPrice;
                    }else{
                        $price = $v->price;
                    }
                    $sum+=(int)$v->count*(int)$price;
                }
            }
        }
        return $sum;
    }

    public static function getAll($pageSize=2)
    {
        $query =  Products::find()->where('(isNew != 0 OR isNew != NULL)')->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }

    public static function getCatalog($catalog, $pageSize=8)
    {
        $query =  Products::find()->where(['category_id' => $catalog])->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
