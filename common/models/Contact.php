<?php
namespace common\models;

use Yii;

class Contact extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'contact';
    }

    public function rules()
    {
        return [
            [['phone', 'email', 'facebook', 'instagram', 'whatsap', 'youtube', 'vk'], 'required'],
            [['phone', 'email', 'facebook', 'instagram', 'whatsap', 'youtube', 'vk'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'email' => 'Email',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'whatsap' => 'Whatsap',
            'youtube' => 'Youtube',
            'vk' => 'Vk',
        ];
    }
}
