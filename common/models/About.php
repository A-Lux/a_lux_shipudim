<?php
namespace common\models;

use Yii;

class About extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'about';
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Описание',
        ];
    }
}
