<?php

namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "vacancy".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $salary
 * @property string $time
 * @property string $location
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'salary', 'payment', 'time'], 'required'],
            [['content'], 'string'],
            [['title', 'salary', 'time', 'payment',  'location'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Описание',
            'salary' => 'Зарплата',
            'payment' => 'Оплата',
            'time' => 'Занятость',
            'location' => 'Адрес',
        ];
    }

    public static function getAll($pageSize=4)
    {
        $query =  Vacancy::find()->orderBy('id DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageParam' => 'vacancy','pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
