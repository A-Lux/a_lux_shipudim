<?php
namespace common\models;

use Yii;

class UserFavorites extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_favorites';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
        ];
    }
}
