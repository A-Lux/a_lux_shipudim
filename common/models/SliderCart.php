<?php
namespace common\models;

use Yii;

class SliderCart extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sliderCart';
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'content'   => 'Контент',
        ];
    }
}
