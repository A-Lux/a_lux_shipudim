<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $image
 * @property string $url
 */
class Partners extends \yii\db\ActiveRecord
{

    public $path = 'images/partners/';

    public static function tableName()
    {
        return 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'url' => 'Страница партнера',
        ];
    }


    public function getImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path.''.$this->image : '/no-image.png';
    }
}
