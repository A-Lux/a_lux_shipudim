<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $image
 * @property string $copyright
 */
class Logo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'images/logo/';

    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Логотип',
            'copyright' => 'Копирайт',
        ];
    }

    public function getImage()
    {
        return '/backend/web/'.$this->path.''.$this->image;
    }

}
