<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $url
 * @property string $image
 * @property int $level
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 */

class Catalog extends \yii\db\ActiveRecord
{
    public $path = 'images/catalog/';

    public static function tableName()
    {
        return 'catalog';
    }

    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['parent_id', 'sort', 'status', 'level'], 'integer'],
            [['created_at', 'metaDesc', 'metaKey'], 'string'],
            [['name', 'url', 'image', 'metaName'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительский каталог',
            'name' => 'Название',
            'url' => 'Url',
            'image' => 'Изображение',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public function upload()
    {
        $time = time();
        $this->image->saveAs($this->path. $time . $this->image->baseName . '.' . $this->image->extension);
        return $time . $this->image->baseName . '.' . $this->image->extension;
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path.''.$this->image : '/no-image.png';
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Catalog::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParent()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'parent_id']);
    }

    public function getParentName(){
        return (isset($this->parent))? $this->parent->name:'Не задано';
    }

    public function getChilds()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id']);
    }

    public function getChildstop()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id'])->limit(24);
    }

    public function getChildsmore()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id'])->offset(23);
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    public function getProductsactive()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id'])->where('status = 1');
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
