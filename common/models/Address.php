<?php

namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $address
 * @property string $phone
 * @property string $mobile_phone
 * @property string $content
 * @property string $longitude
 * @property string $latitude
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'phone', 'longitude', 'latitude'], 'required'],
            [['content'], 'string'],
            [['address', 'phone', 'mobile_phone','longitude', 'latitude'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'mobile_phone' => 'Сотовый телефон',
            'content' => 'Описание',
            'longitude' => 'долгота',
            'latitude' => 'широта',
        ];
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Address::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Address::find()->all(),'id','name');
    }


    public static function getAll($pageSize=8)
    {
        $query =  Address::find()->orderBy('id ASC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageParam' => 'address','pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
