<?php

namespace backend\controllers;

use common\models\ImageUpload;
use Imagine\Image\Box;
use Yii;
use common\models\Catalog;
use backend\models\search\CatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class CatalogController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Catalog();
        $imagine = new \Imagine\Gd\Imagine();
        $path = Yii::getAlias('@backend') . '/web/images/catalog/';

        if ($model->load(Yii::$app->request->post())) {
            $image  = UploadedFile::getInstance($model, 'image');
            $model->url = $model->generateCyrillicToLatin($model->name,'-');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            $imagine->open($path . $model->image)
                ->resize(new Box(508, 435))
                ->save($path . $model->image, ['quality' => 80]);

            if($model->parent_id){
                $model->level = $model->parent->level + 1;
            }else{
                $model->level = 1;
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;
        $imagine = new \Imagine\Gd\Imagine();
        $path = Yii::getAlias('@backend') . '/web/images/catalog/';

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->url = $model->generateCyrillicToLatin($model->name,'-');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    if(file_exists($model->path.$oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }

            $imagine->open($path . $model->image)
                ->resize(new Box(508, 435))
                ->save($path . $model->image, ['quality' => 80]);

            if($model->parent_id){
                $model->level = $model->parent->level + 1;
            }else{
                $model->level = 1;
            }

            if($model->save(false)){
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {

        $model = Catalog::findOne($id);
        $models = Catalog::find()->where('sort > '.$model->sort)->all();

        foreach($models as $v){
            $v->sort--;
            $v->save(false);
        }


        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Catalog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMoveUp($id)
    {
        $model = Catalog::findOne($id);
        if ($model->sort != 1) {
            $sort = $model->sort - 1;
            $model_down = Catalog::find()->where("sort = $sort")->one();
            $model_down->sort += 1;
            $model_down->save(false);

            $model->sort -= 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionMoveDown($id)
    {
        $model = Catalog::findOne($id);
        $model_max_sort = Catalog::find()->orderBy("sort DESC")->one();

        if ($model->id != $model_max_sort->id) {
            $sort = $model->sort + 1;
            $model_up = Catalog::find()->where("sort = $sort")->one();
            $model_up->sort -= 1;
            $model_up->save(false);

            $model->sort += 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }
}
