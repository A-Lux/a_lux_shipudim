<?php

namespace backend\controllers;

use Yii;
use common\models\Geocoords;
use backend\models\search\GeocoordsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
class GeocoordsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new GeocoordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = Geocoords::find()->all();

        return $this->render('index_create', compact('model'));
    }

    public function actionSave_geo()
    {
        $model = new Geocoords();

        $model->coords = serialize($_GET['coords']);
        $model->summ = 1000;
        $model->summ = $_GET['summ'];
        $model->name = $_GET['name'];
        $model->save();

        $model = Geocoords::find()->all();
        return $this->renderAjax('save_geo', compact('model'));
    }

    public function actionUpdateall()
    {
        $model = Geocoords::find()->all();

        return $this->render('index_update', compact('model'));
    }

    public function actionUpdate_geo()
    {
        $model = Geocoords::findOne($_GET['id']);

        $model->coords = serialize($_GET['coords']);
        $model->summ = $_GET['summ'];
        $model->name = $_GET['name'];
        $model->save();

        $model = Geocoords::find()->all();
        return $this->renderAjax('update_geo', compact('model'));
    }

    public function actionDelete_geo()
    {
        Geocoords::deleteAll(['id' => $_GET['id']]);

        $model = Geocoords::find()->all();
        return $this->renderAjax('update_geo', compact('model'));
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Geocoords::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
