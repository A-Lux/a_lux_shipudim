<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.03.2019
 * Time: 20:29
 */

namespace backend\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelTypePay
{
    public static function statusList()
    {
        return [
            1 => 'Наличными курьеру',
            2 => 'Банковская карта курьеру',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 1:
                $class = 'label label-success';
                break;
            case 2:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}