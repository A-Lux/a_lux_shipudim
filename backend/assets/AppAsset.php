<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         "/backend/web/private/bower_components/bootstrap/dist/css/bootstrap.min.css",
         "/backend/web/private/bower_components/font-awesome/css/font-awesome.min.css",
         "/backend/web/private/bower_components/Ionicons/css/ionicons.min.css",
         "/backend/web/private/bower_components/bootstrap-daterangepicker/daterangepicker.css",
         "/backend/web/private/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
         "/backend/web/private/plugins/iCheck/all.css",
         "/backend/web/private/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css",
         "/backend/web/private/plugins/timepicker/bootstrap-timepicker.min.css",
         "/backend/web/private/bower_components/select2/dist/css/select2.min.css",
         "/backend/web/private/dist/css/AdminLTE.min.css",
         "/backend/web/private/dist/css/skins/_all-skins.min.css",
    ];
    public $js = [

         "/backend/web/private/bower_components/jquery/dist/jquery.min.js",
         "/backend/web/private/bower_components/bootstrap/dist/js/bootstrap.min.js",
         "/backend/web/private/bower_components/select2/dist/js/select2.full.min.js",
         "/backend/web/private/plugins/input-mask/jquery.inputmask.js",
         "/backend/web/private/plugins/input-mask/jquery.inputmask.date.extensions.js",
         "/backend/web/private/plugins/input-mask/jquery.inputmask.extensions.js",
         "/backend/web/private/bower_components/moment/min/moment.min.js",
         "/backend/web/private/bower_components/bootstrap-daterangepicker/daterangepicker.js",
         "/backend/web/private/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
         "/backend/web/private/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js",
         "/backend/web/private/plugins/timepicker/bootstrap-timepicker.min.js",
         "/backend/web/private/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
         "/backend/web/private/plugins/iCheck/icheck.min.js",
         "/backend/web/private/bower_components/fastclick/lib/fastclick.js",
         "/backend/web/private/dist/js/adminlte.min.js",
         "/backend/web/private/dist/js/demo.js",

        '/backend/web/js/scripts.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
