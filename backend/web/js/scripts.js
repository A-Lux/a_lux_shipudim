$('body').on('keyup', '.sop_tovary', function () {
    var sop_tovary = [];
    $('input[name="sop_tovary[]"]').each(function () {
        sop_tovary.push($(this).val());
    });

    var id = $('.product_id').val();
    $.ajax({
        type: 'GET',
        url: "/admin/products/sop-tovary",
        data: {name: $('.sop_tovary').val(), id: id, sop_tovary: sop_tovary},
        success: function(html){
            $('.sop_tovary_div').html(html);
        }
    });
});

$('body').on('change', '.sop_tovary_input', function () {
    var label = $(this).next('label');
    if($(this).is(':checked')) {
        $(this).appendTo('.sop_tovary_isset').attr('name', 'sop_tovary[]');
        label.appendTo('.sop_tovary_isset');
    }
});

$('body').on('keyup', '.analogy', function () {
    var analogy = [];
    $('input[name="analogy[]"]').each(function () {
        analogy.push($(this).val());
    });

    var id = $('.product_id').val();
    $.ajax({
        type: 'GET',
        url: "/admin/products/analogy",
        data: {name: $('.analogy').val(), id: id, analogy: analogy},
        success: function(html){
            $('.analogy_div').html(html);
        }
    });
});

$('body').on('change', '.analogy_input', function () {
    var label = $(this).next('label');
    if($(this).is(':checked')) {
        $(this).appendTo('.analogy_isset').attr('name', 'analogy[]');
        label.appendTo('.analogy_isset');
    }
});