<?php

use yii\helpers\Html;

$this->title = 'Создание оплата и доставка';
$this->params['breadcrumbs'][] = ['label' => 'Оплата и Доставка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
