<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = 'Оплата и Доставка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<!--    <p>-->
<!--        --><?//= Html::a('Создать Pay Delivery', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'title',
                'value' => $model->title,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'content',
//                'value' => $model->content,
//                'format' => 'raw',
//            ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
            ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
