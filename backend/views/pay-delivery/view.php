<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Оплата и Доставка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-delivery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'title',
                'value' => $model->title,
                'format' => 'raw',
            ],
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
