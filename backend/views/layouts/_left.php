<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/menu'],'active' => $this->context->id == 'menu'],
                    [
                        'label' => 'Главная страница',
                        'icon' => 'fa fa-home',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Заголовок страницы', 'icon' => 'fa fa-user', 'url' => ['/mainsub'], 'active' => $this->context->id == 'mainsub'],
                            ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/banner'], 'active' => $this->context->id == 'banner'],
                            ['label' => 'О компании', 'icon' => 'fa fa-user', 'url' => ['/about'], 'active' => $this->context->id == 'about'],
                            ['label' => 'Наши адреса', 'icon' => 'fa fa-user', 'url' => ['/address'], 'active' => $this->context->id == 'address'],
                            ['label' => 'Наши партнеры', 'icon' => 'fa fa-user', 'url' => ['/partners'], 'active' => $this->context->id == 'partners'],
                            ['label' => 'Оплата и Доставка', 'icon' => 'fa fa-user', 'url' => ['/pay-delivery'], 'active' => $this->context->id == 'pay-delivery'],
                            ['label' => 'Слайдер', 'icon' => 'fa fa-user', 'url' => ['/slider-cart'], 'active' => $this->context->id == 'slider-cart'],

                        ],
                    ],
                    [
                        'label' => 'Продукция',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Каталог ', 'icon' => 'fa fa-user', 'url' => ['/catalog/index'],'active' => $this->context->id == 'catalog'],
                            ['label' => 'Продукты', 'icon' => 'fa fa-user', 'url' => ['/products/index'], 'active' => $this->context->id == 'products'],
                        ],
                    ],
                    [
                        'label' => 'Заказы',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Заказы ', 'icon' => 'fa fa-user', 'url' => ['/orders/index'],'active' => $this->context->id == 'orders'],
                            ['label' => 'Статус оплаты', 'icon' => 'fa fa-user', 'url' => ['/status-pay/index'], 'active' => $this->context->id == 'status-pay'],
                            ['label' => 'Статус доставки', 'icon' => 'fa fa-user', 'url' => ['/status-progress/index'], 'active' => $this->context->id == 'status-progress'],
                            ['label' => 'Доп. тексты', 'icon' => 'fa fa-user', 'url' => ['/order-info/index'], 'active' => $this->context->id == 'order-info'],
                        ],
                    ],
                    [
                        'label' => 'Вакансия',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Вакансия', 'icon' => 'fa fa-user', 'url' => ['/vacancy'],  'active' => $this->context->id == 'vacancy'],
                            //['label' => 'Откликы на вакансию', 'icon' => 'fa fa-user', 'url' => ['/respond'],  'active' => $this->context->id == 'respond'],
                        ],
                    ],

                    ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/contact'],  'active' => $this->context->id == 'contact'],
                    ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/emailforrequest'],'active' => $this->context->id == 'emailforrequest'],
                    ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/logo'],'active' => $this->context->id == 'logo'],
                    ['label' => 'Координаты доставки', 'icon' => 'fa fa-user', 'url' => ['/geocoords'],'active' => $this->context->id == 'geocoords'],
                ],
            ]
        ) ?>
    </section>

</aside>