<?php

use yii\helpers\Html;

$this->title = 'Создание Geocoords';
$this->params['breadcrumbs'][] = ['label' => 'Geocoords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geocoords-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
