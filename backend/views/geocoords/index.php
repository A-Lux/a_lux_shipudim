<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Geocoords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geocoords-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Редактировать', ['updateall'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'coords',
                'value' => $model->coords,
                'format' => 'raw',
            ],
            'summ',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
