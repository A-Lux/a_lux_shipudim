<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php
        echo $form->field($model, 'name')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full', // basic, standard, full
                'inline' => false, //по умолчанию false
            ],
        ]);
        ?>

        <?php
            echo $form->field($model, 'image')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload' => false ,
                ] ,
                'options' => ['accept' => 'image/*'],
            ]);
        ?>

        <?php
            echo $form->field($model, 'video')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload' => false ,
                ] ,
                'options' => ['accept' => 'video/*'],
            ]);
        ?>

        <?= $form->field($model, 'status')->dropDownList([0 => 'Изображение', 1 => 'Видео']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
