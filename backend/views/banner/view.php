<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\controllers\LabelBanner;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function($data){
                    return $data->name;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return LabelBanner::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'video',
                'value' => !empty($model->video) ? '<video playsinline="playsinline" autoplay="autoplay" muted="" loop="loop" width="300">
                                                        <source src="'.$model->getVideo().'" type="video/mp4">
                                                    </video>' : null,
            ],
        ],
    ]) ?>

</div>
