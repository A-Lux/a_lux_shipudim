<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'facebook',
                'value' => $model->facebook,
                'format' => 'raw',
            ],
            [
                'attribute' => 'instagram',
                'value' => $model->instagram,
                'format' => 'raw',
            ],
            [
                'attribute' => 'whatsap',
                'value' => $model->whatsap,
                'format' => 'raw',
            ],
            [
                'attribute' => 'youtube',
                'value' => $model->youtube,
                'format' => 'raw',
            ],
            [
                'attribute' => 'vk',
                'value' => $model->vk,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
