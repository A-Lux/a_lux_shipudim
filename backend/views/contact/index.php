<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'facebook',
                'value' => $model->facebook,
                'format' => 'raw',
            ],
            [
                'attribute' => 'instagram',
                'value' => $model->instagram,
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
            ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
