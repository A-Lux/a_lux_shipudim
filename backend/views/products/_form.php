<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

?>
<div class="products-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'articul')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-4 col-xs-3" style="text-align: center">
            <div style="margin-top:20px;">
                <img src="/<?=$model->path?><?=$model->image?>" alt="" style="width:200px;">
            </div>
        </div>
    </div>

    <?php
    echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false
        ]),
    ]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        \common\models\Catalog::getList(), ['prompt' => ' ']
    ) ?>

    <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно'], ['prompt' => '']) ?>

    <?= $form->field($model, 'isNew')->dropDownList([0 => 'Старая', 1 => 'Новая'], ['prompt' => '']) ?>

    <?= $form->field($model, 'isDiscount')->dropDownList([0 => 'Не действует', 1 => 'Действует'], ['prompt' => '']) ?>

    <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>


    <div class="form-group" style="padding-top: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
