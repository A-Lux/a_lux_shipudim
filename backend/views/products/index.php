<?php


use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                 'attribute' => 'category_id',
                 'value' => function ($model) {
                     return $model->category->name;
                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'isDiscount',
                'filter' => \backend\controllers\LabelDiscount::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelDiscount::statusLabel($model->isDiscount);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            [
                 'value' => function ($model) {
                     return
                         Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                         Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
