<?php

use yii\helpers\Html;

$this->title = 'Редактирование Order Info: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Тексты о заказе', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="order-info-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
