<?php

use yii\helpers\Html;

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Тексты о заказе', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
