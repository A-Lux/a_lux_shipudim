<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->product_id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //['attribute'=>'user_id', 'value'=>function($model){ return $model->user->name." ".$model->user->surname.' '.$model->user->father;}],
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            'product_id',
            ['attribute' => 'statusPay', 'value' => $model->payStatus->name],

            ['attribute' => 'statusProgress', 'value' => $model->progressStatus->name],
            'sum',
            'change',
            [
                'attribute' => 'address',
                'value' => $model->address,
                'format' => 'raw',
            ],
            [
                'attribute' => 'street',
                'value' => $model->street,
                'format' => 'raw',
            ],
            [
                'attribute' => 'commentOrder',
                'value' => $model->commentOrder,
                'format' => 'raw',
            ],
            [
                'attribute' => 'typePay',
                'filter' => \backend\controllers\LabelTypePay::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelTypePay::statusLabel($model->typePay);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

    <h2>Продукты заказа:</h2>
    <br>
    <?php foreach ($products as $v) :?>
        <div style="float: left;margin-right: 50px;">
            <p style="width: 200px"><?=$v->name;?><b>  X <?=$v->count?></b></p>
            <a href="/product/<?=$v->id?>">
                <img src="<?=$v->getImage()?>"  width="100">
            </a>

        </div>
    <?php endforeach;?>
</div>
