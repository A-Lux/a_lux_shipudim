<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<!--    <p>-->
<!--        --><?//= Html::a('Создать Orders', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            'product_id',

            ['attribute' => 'statusProgress', 'value' => 'progressStatusName', 'filter' => \common\models\StatusProgress::getList()],

            ['attribute' => 'statusPay', 'value' => 'payStatusName', 'filter' => \common\models\StatusPay::getList()],
            [
                'attribute' => 'typePay',
                'filter' => \backend\controllers\LabelTypePay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelTypePay::statusLabel($model->typePay);
                },
                'format' => 'raw',
            ],
            'created_at',

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
            ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
