<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'statusPay')->dropDownList(\common\models\StatusPay::getList(),
        ['prompt' => ' ']) ?>

    <?= $form->field($model, 'statusProgress')->dropDownList(\common\models\StatusProgress::getList(),
        ['prompt' => ' ']) ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'change')->textInput() ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'commentOrder')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
