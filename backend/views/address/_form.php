<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<div class="address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false
        ]),
    ]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>


    <div id="map" style="width: 100%; height: 250px"></div>

    <script>
        ymaps.ready(init);
        var center_map = [0, 0];
        var map = "";
        function init() {
            map = new ymaps.Map('map', {
                center: [43.238293, 76.945465],
                zoom: 12,
            });

            <?php if(empty($model->longitude)&&empty($model->latitude)){ ?>
            var myGeocoder = ymaps.geocode("Алматы");
            myGeocoder.then(
                function (res) {
//                    map.geoObjects.add(res.geoObjects);
                    var street = res.geoObjects.get(0);
                    var coords = street.geometry.getCoordinates();
                    map.setCenter(coords);
                },
                function (err) {

                }
            );
            <?php }else{ ?>
            map.setCenter([<?=$model->latitude?>, <?=$model->longitude?>]);

            map.geoObjects.add(new ymaps.Placemark([<?=$model->latitude?>, <?=$model->longitude?>], {
                balloonContent: ''
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }));
            <?php } ?>
          
            map.events.add('click', function (e) {
                map.geoObjects.removeAll();
                var coords = e.get('coords');
                map.geoObjects.add(new ymaps.Placemark(coords, {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#0095b6'
                }));



                $("#address-latitude").val(coords[0].toPrecision(9));
                $("#address-longitude").val(coords[1].toPrecision(9));


            });
        }
    </script>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
