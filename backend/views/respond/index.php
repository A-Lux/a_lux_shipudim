<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = 'Откликы на вакансии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="respond-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'vacancy_id',
            [
                'attribute' => 'vacancy_id',
                'value' => function ($model) {
                    return $model->vacancy->title;
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],


                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
            ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
