<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->vacancy->title;
$this->params['breadcrumbs'][] = ['label' => 'Откликы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="respond-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>
    <?php $files = $model->getFilePath(); ?>
    <?php $filelink = $files->file->file;?>
    <?php if($files != null) $file = ['attribute'=>'Файл','value' => Html::a('Посмотреть файл',$files,['target'=>'_blank']),'format' => 'raw' ];
    else $file =  ['attribute'=>'Файл','value' => '']?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'vacancy_id', 'value'=>function($model){ return $model->vacancyName;}],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'text',
                'value' => $model->text,
                'format' => 'raw',
            ],
            $file,
            //'created_at',
        ],
    ]) ?>

</div>
