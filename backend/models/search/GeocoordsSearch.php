<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Geocoords;

class GeocoordsSearch extends Geocoords
{
    public function rules()
    {
        return [
            [['id', 'summ'], 'integer'],
            [['coords'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Geocoords::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'summ' => $this->summ,
        ]);

        $query->andFilterWhere(['like', 'coords', $this->coords]);

        return $dataProvider;
    }
}
