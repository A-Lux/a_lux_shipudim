<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

class OrdersSearch extends Orders
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'product_id', 'statusPay', 'statusProgress', 'sum', 'change', 'typePay', 'typeDelivery'], 'integer'],
            [['username', 'phone', 'address', 'street', 'commentOrder', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'product_id' => $this->product_id,
            'statusPay' => $this->statusPay,
            'statusProgress' => $this->statusProgress,
            'sum' => $this->sum,
            'change' => $this->change,
            'typePay' => $this->typePay,
            'typeDelivery' => $this->typeDelivery,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'commentOrder', $this->commentOrder]);

        $query->orderBy('id DESC');

        return $dataProvider;
    }
}
