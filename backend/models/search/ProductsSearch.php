<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

class ProductsSearch extends Products
{
    public function rules()
    {
        return [
            [['id', 'price', 'status', 'isNew', 'isDiscount', 'newPrice', 'category_id'], 'integer'],
            [['name', 'articul', 'url', 'image', 'content', 'metaName', 'metaDesc', 'metaKey'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'status' => $this->status,
            'isNew' => $this->isNew,
            'isHit' => $this->isHit,
            'isDiscount' => $this->isDiscount,
            'newPrice' => $this->newPrice,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'articul', $this->articul])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'metaName', $this->metaName])
            ->andFilterWhere(['like', 'metaDesc', $this->metaDesc])
            ->andFilterWhere(['like', 'metaKey', $this->metaKey]);

        $query->orderBy('sort ASC');

        return $dataProvider;
    }
}
